/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('login', {
            url: '/login',
            templateUrl: 'views/login_view/login.html',
             data: { pageTitle: 'Login' },
            controller: 'AuthLoginController'
        })
        .state('logout', {
            url: '/logout',
            controller: 'AuthLogoutController'
        })
        .state('forbidden', {
            url: '/forbidden',
            templateUrl: 'views/forbidden.html'
        })

        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "views/common/content.html",
        })




        .state('index.lista_eventos', {
            url: '/lista_eventos',
            templateUrl: 'views/eventos_view/lista_eventos.html',
            data: { pageTitle: 'Lista de eventos' },
            controller: 'ListaEventosController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('index.alta_evento', {
            url: '/alta_evento',
            templateUrl: 'views/eventos_view/form_evento.html',
            data: { pageTitle: 'Nuevo evento' },
            controller: 'AltaEventoController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        },
                        {
                            name: 'ui.date',
                            files: ['librerias/jquery-ui/themes/smoothness/jquery-ui.css','librerias/jquery-ui/ui/i18n/datepicker-es.js','librerias/angular-ui-date/src/date.js']
                        },
                        {
                            name: 'NgSwitchery',
                            files: ['librerias/switchery/dist/switchery.min.css','librerias/switchery/dist/switchery.min.js','librerias/ng-switchery/src/ng-switchery.js']
                        },
                        {
                            name: 'summernote',
                            files: ['librerias/summernote/dist/summernote.css','librerias/summernote/dist/summernote-bs3.css','librerias/summernote/dist/summernote.min.js','librerias/angular-summernote/dist/angular-summernote.min.js']
                        },
                        {
                            name: 'ui.grid',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.edit',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.cellNav',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.autoResize',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            files: ['librerias/angular-ui-grid/ui-grid.min.css']
                        }
                    ]);
                }
            }
        })
        .state('index.edita_evento', {
            url: '/edita_evento/:id',
            templateUrl: 'views/eventos_view/form_evento.html',
            data: { pageTitle: 'Editar evento' },
            controller: 'EditaEventoController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        },
                        {
                            name: 'ui.date',
                            files: ['librerias/jquery-ui/themes/smoothness/jquery-ui.css','librerias/jquery-ui/ui/i18n/datepicker-es.js','librerias/angular-ui-date/src/date.js']
                        },
                        {
                            name: 'NgSwitchery',
                            files: ['librerias/switchery/dist/switchery.min.css','librerias/switchery/dist/switchery.min.js','librerias/ng-switchery/src/ng-switchery.js']
                        },
                        {
                            name: 'summernote',
                            files: ['librerias/summernote/dist/summernote.css','librerias/summernote/dist/summernote-bs3.css','librerias/summernote/dist/summernote.min.js','librerias/angular-summernote/dist/angular-summernote.min.js']
                        },
                        {
                            name: 'ui.grid',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.edit',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.cellNav',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.autoResize',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            files: ['librerias/angular-ui-grid/ui-grid.min.css']
                        }
                    ]);
                }
            }
        })




        .state('index.lista_articulos', {
            url: '/lista_articulos',
            templateUrl: 'views/articulos_view/lista_articulos.html',
            data: { pageTitle: 'Lista de artículos' },
            controller: 'ListaArticulosController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('index.alta_articulo', {
            url: '/alta_articulo',
            templateUrl: 'views/articulos_view/form_articulo.html',
            data: { pageTitle: 'Nuevo artículo' },
            controller: 'AltaArticuloController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        },
                        {
                            name: 'ui.date',
                            files: ['librerias/jquery-ui/themes/smoothness/jquery-ui.css','librerias/jquery-ui/ui/i18n/datepicker-es.js','librerias/angular-ui-date/src/date.js']
                        },
                        {
                            name: 'NgSwitchery',
                            files: ['librerias/switchery/dist/switchery.min.css','librerias/switchery/dist/switchery.min.js','librerias/ng-switchery/src/ng-switchery.js']
                        },
                        {
                            name: 'summernote',
                            files: ['librerias/summernote/dist/summernote.css','librerias/summernote/dist/summernote-bs3.css','librerias/summernote/dist/summernote.min.js','librerias/angular-summernote/dist/angular-summernote.min.js']
                        }
                    ]);
                }
            }
        })
        .state('index.edita_articulo', {
            url: '/edita_articulo/:id',
            templateUrl: 'views/articulos_view/form_articulo.html',
            data: { pageTitle: 'Editar artículo' },
            controller: 'EditaArticuloController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        },
                        {
                            name: 'ui.date',
                            files: ['librerias/jquery-ui/themes/smoothness/jquery-ui.css','librerias/jquery-ui/ui/i18n/datepicker-es.js','librerias/angular-ui-date/src/date.js']
                        },
                        {
                            name: 'NgSwitchery',
                            files: ['librerias/switchery/dist/switchery.min.css','librerias/switchery/dist/switchery.min.js','librerias/ng-switchery/src/ng-switchery.js']
                        },
                        {
                            name: 'summernote',
                            files: ['librerias/summernote/dist/summernote.css','librerias/summernote/dist/summernote-bs3.css','librerias/summernote/dist/summernote.min.js','librerias/angular-summernote/dist/angular-summernote.min.js']
                        }
                    ]);
                }
            }
        })




        .state('oferta_ed', {
            abstract: true,
            url: "/oferta_ed",
            templateUrl: "views/common/content.html",
        })
        .state('oferta_ed.modalidades', {
            url: '/modalidades',
            templateUrl: 'views/modalidades_view/lista_modalidades.html',
            data: { pageTitle: 'Modalidades carreras' },
            controller: 'ListaModalidadesController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('oferta_ed.carreras', {
            url: '/carreras',
            templateUrl: 'views/carreras_view/lista_carreras.html',
            data: { pageTitle: 'Catálogo de carreras' },
            controller: 'ListaCarrerasController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        },
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('oferta_ed.alta_carrera', {
            url: '/alta_carrera',
            templateUrl: 'views/carreras_view/form_datos_carrera.html',
            data: { pageTitle: 'Alta carrera' },
            controller: 'AltaCarreraController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        },
                        {
                            name: 'summernote',
                            files: ['librerias/summernote/dist/summernote.css','librerias/summernote/dist/summernote-bs3.css','librerias/summernote/dist/summernote.min.js','librerias/angular-summernote/dist/angular-summernote.min.js']
                        }
                    ]);
                }
            }
        })
        .state('oferta_ed.edita_carrera', {
            url: '/edita_carrera/:id',
            templateUrl: 'views/carreras_view/form_datos_carrera.html',
            data: { pageTitle: 'Editar carrera' },
            controller: 'EditaCarreraController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'summernote',
                            files: ['librerias/summernote/dist/summernote.css','librerias/summernote/dist/summernote-bs3.css','librerias/summernote/dist/summernote.min.js','librerias/angular-summernote/dist/angular-summernote.min.js']
                        }
                    ]);
                }
            }
        })




        .state('galerias', {
            abstract: true,
            url: "/galerias",
            templateUrl: "views/common/content.html",
        })
        .state('galerias.lista_fotos', {
            url: '/lista_fotos',
            templateUrl: 'views/fotos_view/lista_fotos.html',
            data: { pageTitle: 'Lista de fotos' },
            controller: 'ListaFotosController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('galerias.alta_foto', {
            url: '/alta_foto',
            templateUrl: 'views/fotos_view/form_foto.html',
            data: { pageTitle: 'Nueva foto' },
            controller: 'AltaFotoController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        }
                    ]);
                }
            }
        })
        .state('galerias.edita_foto', {
            url: '/edita_foto/:id',
            templateUrl: 'views/fotos_view/form_foto.html',
            data: { pageTitle: 'Editar foto' },
            controller: 'EditaFotoController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        }
                    ]);
                }
            }
        })
        .state('galerias.lista_videos', {
            url: '/lista_videos',
            templateUrl: 'views/videos_view/lista_videos.html',
            data: { pageTitle: 'Lista de videos' },
            controller: 'ListaVideoController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('galerias.alta_video', {
            url: '/alta_video',
            templateUrl: 'views/videos_view/form_video.html',
            data: { pageTitle: 'Nuevo video' },
            controller: 'AltaVideoController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        }
                    ]);
                }
            }
        })
        .state('galerias.edita_video', {
            url: '/edita_video/:id',
            templateUrl: 'views/videos_view/form_video.html',
            data: { pageTitle: 'Editar video' },
            controller: 'EditaVideoController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        }
                    ]);
                }
            }
        })


        .state('utrm_conectado', {
            abstract: true,
            url: "/utrm_conectado",
            templateUrl: "views/common/content.html",
        })
        .state('utrm_conectado.clasif_utrm_conectado', {
            url: '/clasif_utrm_conectado',
            templateUrl: 'views/videosutrm_view/lista_clasif_utrm_conectado.html',
            data: { pageTitle: 'Modalidades carreras' },
            controller: 'ListaClasifUTRMController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('utrm_conectado.lista_videos_utrm_conectado', {
            url: '/lista_videos_utrm_conectado',
            templateUrl: 'views/videosutrm_view/lista_videosutrm.html',
            data: { pageTitle: 'Lista de videos' },
            controller: 'ListaVideoUTRMController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('utrm_conectado.alta_video_utrm_conectado', {
            url: '/alta_video_utrm_conectado',
            templateUrl: 'views/videosutrm_view/form_videoutrm.html',
            data: { pageTitle: 'Nuevo video' },
            controller: 'AltaVideoUTRMController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        }
                    ]);
                }
            }
        })
        .state('utrm_conectado.edita_video_utrm_conectado', {
            url: '/edita_video_utrm_conectado/:id',
            templateUrl: 'views/videosutrm_view/form_videoutrm.html',
            data: { pageTitle: 'Editar video' },
            controller: 'EditaVideoUTRMController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'naif.base64',
                            files: ['librerias/angular-base64-upload/dist/angular-base64-upload.min.js']
                        }
                    ]);
                }
            }
        })



 
        .state('index.carga_academica', {
            url: '/carga_academica',
            templateUrl: 'views/carga_acad_view/lista_alumnos_cargas.html',
            data: { pageTitle: 'Carga académica de los alumnos' },
            controller: 'ListaAlumnosCargasController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('index.importar_carga', {
            url: '/importar_carga',
            templateUrl: 'views/carga_acad_view/form_importar_carga.html',
            data: { pageTitle: 'Importar carga académica' },
            controller: 'ImportaCargaController',
            authenticate: true,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ui.grid',
                            files: ['librerias/js-xlsx/dist/xlsx.full.min.js']
                        },
                        {
                            name: 'ui.grid',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.edit',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.cellNav',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            name: 'ui.grid.autoResize',
                            files: ['librerias/angular-ui-grid/ui-grid.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            files: ['librerias/angular-ui-grid/ui-grid.min.css']
                        },
                        {
                            files: ['librerias/sweetalert2/dist/sweetalert2.min.js', 'librerias/sweetalert2/dist/sweetalert2.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['librerias/angular-sweetalert/SweetAlert.min.js']
                        }
                    ]);
                }
            }
        })
        
    $urlRouterProvider.otherwise("login");
    //$urlRouterProvider.otherwise("/index/main");
}
angular
    .module('inspinia')
    .config(config)
    .run(function($rootScope, $state, localStorageService) {

            $rootScope.$on('$stateChangeStart', function(event, next) {

            if (localStorageService.get('usuario') !== undefined)
                    $rootScope.currentUser = localStorageService.get('usuario');

              // redirect to login page if not logged in
              if (next.authenticate && !$rootScope.currentUser) {
                event.preventDefault(); //prevent current page from loading
                $state.go('login');
              }
            });

        $rootScope.$state = $state;

    });
