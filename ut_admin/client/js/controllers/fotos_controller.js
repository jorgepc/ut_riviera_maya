function ListaFotosController($scope, Galeria_foto, SweetAlert, $state, $stateParams) {

        $scope.$parent.$parent.main.titulo_seccion = 'Fotos';

        $scope.mostrarbtnLimpiar = false;

        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.begin = 0;
        $scope.end = 5;
        $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id };

        Galeria_foto.count({
            where: $scope.condicion
        })
        .$promise
        .then(function(resp) {
            $scope.totalItems = resp.count;
            if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
            if($scope.totalItems == 0)
            {
                $scope.begin = -1;
                $scope.end = 0;
                $scope.inactivos = 0;
                $scope.activos = 0;
            }
        });


        $scope.lista_fotos = Galeria_foto.find({
              filter: {
                  where: $scope.condicion,
                  order: 'titulo ASC',
                  limit: $scope.numPerPage,
                  skip: $scope.currentPage - 1
              }
        });


        $scope.pageChanged = function () {

              $scope.begin = ($scope.currentPage - 1) * $scope.numPerPage;
              $scope.end = $scope.begin + $scope.numPerPage;
              if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
        
              $scope.lista_fotos = Galeria_foto.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'titulo ASC',
                        limit: $scope.numPerPage,
                        skip:  $scope.begin
                    }
              });
        }

        $scope.muestraResultadosBusqueda = function() {

              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {and: [
                      {campusId: $scope.currentUser.campus_pertenece_id},
                      {titulo: {
                          like: $scope.nombre_buscar+ '.*', 
                          options: 'i'
                      }}
              ]};

              Galeria_foto.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                    $scope.totalItems = resp.count;
                    if($scope.end > $scope.totalItems)
                        $scope.end = $scope.totalItems;
                    if($scope.totalItems == 0) {
                        $scope.begin = -1;
                        $scope.end = 0;              
                    }
              });                                  

              $scope.lista_fotos = Galeria_foto.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'titulo ASC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1
                    }
              });

              $scope.mostrarbtnLimpiar = true;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.limpiaBusqueda = function() {

              $scope.nombre_buscar = '';
              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id};

              Galeria_foto.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                  $scope.totalItems = resp.count;
                  if($scope.end > $scope.totalItems)
                      $scope.end = $scope.totalItems;
                  if($scope.totalItems == 0) {
                      $scope.begin = -1;
                      $scope.end = 0;              
                  }
              });                                  

              $scope.lista_fotos = Galeria_foto.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'titulo ASC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1
                    }
              });

              $scope.mostrarbtnLimpiar = false;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.muestraDatosRegistroSeleccionado = function(RegistroSeleccionado) {
              var index = $scope.lista_fotos.indexOf(RegistroSeleccionado);
              $scope.DatosFotoActual = $scope.lista_fotos[index];
              $scope.client = 2;
              $scope.selectedRow = index;
        };


        $scope.altaRegistro = function() {
              $state.go('galerias.alta_foto');
        };


        $scope.eliminaFoto = function(RegistroSeleccionado) {

              $scope.idx_registro = $scope.lista_fotos.indexOf(RegistroSeleccionado);

              swal({
                title: "Eliminar foto",
                html: '¿Desea eliminar la foto <strong>'+RegistroSeleccionado.titulo +'</strong> ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
              }, function(){
                    swal.disableButtons();

                    Galeria_foto.deleteById({ id: RegistroSeleccionado.id })
                    .$promise
                    .then(function() {

                            $scope.client = 1;
                            $scope.selectedRow = undefined;

                            $scope.totalItems--;
                            $scope.currentPage = 1;
                            $scope.begin = 0;
                            $scope.end = 5;

                            if($scope.end > $scope.totalItems)
                                $scope.end = $scope.totalItems;
                            if($scope.totalItems == 0) {
                                $scope.begin = -1;
                                $scope.end = 0;              
                            }

                            $scope.lista_fotos = Galeria_foto.find({
                                  filter: {
                                      where: $scope.condicion,
                                      order: 'titulo ASC',
                                      limit: $scope.numPerPage,
                                      skip: $scope.currentPage - 1
                                  }
                            });

                            swal('Foto eliminada', '', 'success');
                    });

              });

        };
};



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function AltaFotoController($scope, Galeria_foto, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Nuevo foto';
      $scope.txt_boton = 'Agregar';

      $scope.temp = {
        foto: {}
      };

      $scope.foto = {
          titulo         : "",
          data           : "img/placeholder.png"
      };

      $scope.borraFoto = function () {
            $scope.foto.data = 'img/placeholder.png';
      }

      $scope.guardaDatos = function() {

            if($scope.foto.data === 'img/placeholder.png')
              $scope.foto.data = '';

            if($scope.temp.foto != undefined)
            {
                if($scope.foto.data !== $scope.temp.foto.base64)
                  $scope.foto.data = "data:"+$scope.temp.foto.filetype+";base64,"+$scope.temp.foto.base64;
            }

            Galeria_foto
              .create({
                  titulo         :$scope.foto.titulo,
                  fecha_creacion :Date(),
                  data           :$scope.foto.data,
                  campusId       :$scope.currentUser.campus_pertenece_id
              })
              .$promise
              .then(function(nuevo_cliente) {
                    $state.go('galerias.lista_fotos');
              });

      };

      $scope.cancelaForm = function() {
          $state.go('galerias.lista_fotos');
      };

};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function EditaFotoController($scope, $q, Galeria_foto, $stateParams, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Editar foto';
      $scope.txt_boton = 'Guardar';

      $scope.temp = {
        foto: {}
      };

      $q
        .all([
          Galeria_foto.findById({ 
              id: $stateParams.id
          }).$promise
        ])
        .then(function(data) {
            $scope.foto = data[0];
      
            if($scope.foto.data === '' || $scope.foto.data === undefined)
              $scope.foto.data = 'img/placeholder.png';
        });


      $scope.borraFoto = function () {
            $scope.foto.data = 'img/placeholder.png';
      }

      $scope.guardaDatos = function() {

            if($scope.foto.data === 'img/placeholder.png')
              $scope.foto.data = '';

            if($scope.temp.foto.base64 != undefined)
            {
                if($scope.foto.data !== $scope.temp.foto.base64)
                  $scope.foto.data = "data:"+$scope.temp.foto.filetype+";base64,"+$scope.temp.foto.base64;
            }
            
            Galeria_foto.prototype$updateAttributes({ id: $scope.foto.id }, $scope.foto)
            .$promise
            .then(function(cliente) {
                  $state.go('galerias.lista_fotos');
            })
            .catch(function(error) {
              if(error.status == 413)
                alert("El tamaño del archivo de imagen es muy grande");
            });

      };

      $scope.cancelaForm = function() {
          $state.go('galerias.lista_fotos');
      };

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

angular
    .module('inspinia')
    .controller('ListaFotosController' , ['$scope', 'Galeria_foto', 'SweetAlert', '$state', '$stateParams', ListaFotosController])
    .controller('AltaFotoController'   , ['$scope', 'Galeria_foto', '$state', AltaFotoController])
    .controller('EditaFotoController'  , ['$scope', '$q', 'Galeria_foto', '$stateParams', '$state',  EditaFotoController])