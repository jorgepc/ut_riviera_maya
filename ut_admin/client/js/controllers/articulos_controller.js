function ListaArticulosController($scope, Articulo, SweetAlert, $state, $stateParams) {

        $scope.$parent.$parent.main.titulo_seccion = 'Artículos';

        $scope.mostrarbtnLimpiar = false;

        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.begin = 0;
        $scope.end = 5;
        $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id };

        Articulo.count({
            where: $scope.condicion
        })
        .$promise
        .then(function(resp) {
            $scope.totalItems = resp.count;
            if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
            if($scope.totalItems == 0)
            {
                $scope.begin = -1;
                $scope.end = 0;
                $scope.inactivos = 0;
                $scope.activos = 0;
            }
        });


        $scope.lista_articulos = Articulo.find({
              filter: {
                  where: $scope.condicion,
                  order: 'fecha DESC',
                  limit: $scope.numPerPage,
                  skip: $scope.currentPage - 1,
                  fields:['id','titulo','descripcion','contenido','img_principal','fecha','activo']
              }
        });


        $scope.pageChanged = function () {

              $scope.begin = ($scope.currentPage - 1) * $scope.numPerPage;
              $scope.end = $scope.begin + $scope.numPerPage;
              if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
        
              $scope.lista_articulos = Articulo.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'fecha DESC',
                        limit: $scope.numPerPage,
                        skip:  $scope.begin,
                        fields:['id','titulo','descripcion','contenido','img_principal','fecha','activo']
                    }
              });
        }

        $scope.muestraResultadosBusqueda = function() {

              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {and: [
                      {campusId: $scope.currentUser.campus_pertenece_id},
                      {titulo: {
                          like: $scope.nombre_buscar+ '.*', 
                          options: 'i'
                      }}
              ]};

              Articulo.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                    $scope.totalItems = resp.count;
                    if($scope.end > $scope.totalItems)
                        $scope.end = $scope.totalItems;
                    if($scope.totalItems == 0) {
                        $scope.begin = -1;
                        $scope.end = 0;              
                    }
              });                                  

              $scope.lista_articulos = Articulo.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'fecha DESC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1,
                        fields:['id','titulo','descripcion','contenido','img_principal','fecha','activo']
                    }
              });

              $scope.mostrarbtnLimpiar = true;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.limpiaBusqueda = function() {

              $scope.nombre_buscar = '';
              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id};

              Articulo.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                  $scope.totalItems = resp.count;
                  if($scope.end > $scope.totalItems)
                      $scope.end = $scope.totalItems;
                  if($scope.totalItems == 0) {
                      $scope.begin = -1;
                      $scope.end = 0;              
                  }
              });                                  

              $scope.lista_articulos = Articulo.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'fecha DESC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1,
                        fields:['id','titulo','descripcion','contenido','img_principal','fecha','activo']
                    }
              });

              $scope.mostrarbtnLimpiar = false;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.muestraDatosRegistroSeleccionado = function(RegistroSeleccionado) {
              var index = $scope.lista_articulos.indexOf(RegistroSeleccionado);
              $scope.DatosRegistroActual = $scope.lista_articulos[index];
              $scope.client = 2;
              $scope.selectedRow = index;
        };


        $scope.altaRegistro = function() {
              $state.go('index.alta_articulo');
        };


        $scope.activarRegistro = function(RegistroSeleccionado) {

              $scope.idx_registro = $scope.lista_articulos.indexOf(RegistroSeleccionado);
              
              swal({
                title: "Activar artículo",
                html: '¿Desea activar art&iacute;culo <strong>'+RegistroSeleccionado.titulo +'</strong> ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Activar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
              }, function(){
                    swal.disableButtons();

                    Articulo.prototype$updateAttributes({ id: RegistroSeleccionado.id }, {activo: true})
                    .$promise
                    .then(function(cliente) {
                        $scope.lista_articulos[$scope.idx_registro].activo = true;
                        swal('Artículo activado', '', 'success');
                    });
              });

        };


        $scope.desactivarRegistro = function(RegistroSeleccionado) {

              $scope.idx_registro = $scope.lista_articulos.indexOf(RegistroSeleccionado);

              swal({
                title: "Desactivar artículo",
                html: '¿Desea desctivar el art&iacute;culo <strong>'+RegistroSeleccionado.titulo +'</strong> ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Desactivar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
              }, function(){
                    swal.disableButtons();
                    Articulo.prototype$updateAttributes({ id: RegistroSeleccionado.id }, {activo: false})
                    .$promise
                    .then(function(articulo) {
                        $scope.lista_articulos[$scope.idx_registro].activo = false;
                        swal('artículo desactivado', '', 'success');                            
                    });
              });
        };


        $scope.eliminaArticulo = function(RegistroSeleccionado) {

              $scope.idx_registro = $scope.lista_articulos.indexOf(RegistroSeleccionado);

              swal({
                title: "Eliminar artículo",
                html: '¿Desea eliminar el art&iacute;culo <strong>'+RegistroSeleccionado.titulo +'</strong> ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
              }, function(){
                    swal.disableButtons();

                    Articulo.deleteById({ id: RegistroSeleccionado.id })
                    .$promise
                    .then(function() {

                            $scope.client = 1;
                            $scope.selectedRow = undefined;

                            $scope.totalItems--;
                            $scope.currentPage = 1;
                            $scope.begin = 0;
                            $scope.end = 5;

                            if($scope.end > $scope.totalItems)
                                $scope.end = $scope.totalItems;
                            if($scope.totalItems == 0) {
                                $scope.begin = -1;
                                $scope.end = 0;              
                            }

                            $scope.lista_articulos = Articulo.find({
                                  filter: {
                                      where: $scope.condicion,
                                      order: 'fecha DESC',
                                      limit: $scope.numPerPage,
                                      skip: $scope.currentPage - 1,
                                      fields:['id','titulo','descripcion','contenido','img_principal','fecha','activo']
                                  }
                            });

                            swal('Artículo eliminado', '', 'success');
                    });

              });

        };
};



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function AltaArticuloController($scope, Articulo, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Nuevo artículo';
      $scope.txt_boton = 'Agregar';

      $scope.temp = {
        foto_principal: {}
      };

      $scope.articulo = {
          titulo        : "",
          descripcion   : "",
          contenido     : "",
          img_principal : "img/placeholder.png",
          activo        : true
      };


      $scope.borraFoto = function () {
            $scope.articulo.img_principal = 'img/placeholder.png';
      }

      $scope.guardaDatos = function() {

            if($scope.articulo.img_principal === 'img/placeholder.png')
              $scope.articulo.img_principal = '';

            if($scope.temp.img_principal != undefined)
            {
                if($scope.articulo.img_principal !== $scope.temp.img_principal.base64)
                  $scope.articulo.img_principal = "data:"+$scope.temp.img_principal.filetype+";base64,"+$scope.temp.img_principal.base64;
            }

            Articulo
              .create({
                  titulo        :$scope.articulo.titulo,
                  descripcion   :$scope.articulo.descripcion,
                  contenido     :$scope.articulo.contenido,
                  img_principal :$scope.articulo.img_principal,
                  fecha         :Date(),
                  activo        :$scope.articulo.activo,
                  campusId      :$scope.currentUser.campus_pertenece_id
              })
              .$promise
              .then(function(nuevo_cliente) {
                    $state.go('index.lista_articulos');
              });

      };

      $scope.cancelaForm = function() {
          $state.go('index.lista_articulos');
      };

};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function EditaArticuloController($scope, $q, Articulo, $stateParams, $state) {

      $scope.titulo = 'Editar artículo';
      $scope.txt_boton = 'Guardar';
      $scope.btn_buscar_cliente = false;

      $scope.temp = {
        img_principal: {}
      };

      $q
        .all([
          Articulo.findById({ 
              id: $stateParams.id
          }).$promise
        ])
        .then(function(data) {
            $scope.articulo = data[0];
      
            if($scope.articulo.img_principal === '' || $scope.articulo.img_principal === undefined)
              $scope.articulo.img_principal = 'img/placeholder.png';
        });


      $scope.borraFoto = function () {
            $scope.articulo.img_principal = 'img/placeholder.png';
      }

      $scope.guardaDatos = function() {

            if($scope.articulo.img_principal === 'img/placeholder.png')
              $scope.articulo.img_principal = '';

            if($scope.temp.img_principal.base64 != undefined)
            {
                if($scope.articulo.img_principal !== $scope.temp.img_principal.base64)
                  $scope.articulo.img_principal = "data:"+$scope.temp.img_principal.filetype+";base64,"+$scope.temp.img_principal.base64;
            }
            
            Articulo.prototype$updateAttributes({ id: $scope.articulo.id }, $scope.articulo)
            .$promise
            .then(function(cliente) {
                  $state.go('index.lista_articulos');
            })
            .catch(function(error) {
              if(error.status == 413)
                alert("El tamaño del archivo de imagen es muy grande");
            });

      };

      $scope.cancelaForm = function() {
          $state.go('index.lista_articulos');
      };

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

angular
    .module('inspinia')
    .controller('ListaArticulosController' , ['$scope', 'Articulo', 'SweetAlert', '$state', '$stateParams', ListaArticulosController])
    .controller('AltaArticuloController'   , ['$scope', 'Articulo', '$state', AltaArticuloController])
    .controller('EditaArticuloController'  , ['$scope', '$q', 'Articulo', '$stateParams', '$state',  EditaArticuloController])