function ListaEventosController($scope, Evento, SweetAlert, $sce, $state, $stateParams) {

        $scope.$parent.$parent.main.titulo_seccion = 'Eventos';

        $scope.mostrarbtnLimpiar = false;

        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.begin = 0;
        $scope.end = 5;
        $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id };

        Evento.count({
            where: $scope.condicion
        })
        .$promise
        .then(function(resp) {
            $scope.totalItems = resp.count;
            if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
            if($scope.totalItems == 0)
            {
                $scope.begin = -1;
                $scope.end = 0;
                $scope.inactivos = 0;
                $scope.activos = 0;
            }
        });


        $scope.lista_eventos = Evento.find({
              filter: {
                  where: $scope.condicion,
                  order: 'fecha_inicio DESC',
                  limit: $scope.numPerPage,
                  skip: $scope.currentPage - 1,
                  include: {
                      relation: 'programa',
                      scope: {
                          order: 'orden ASC'
                      }
                  }
              }
        });


        $scope.pageChanged = function () {

              $scope.begin = ($scope.currentPage - 1) * $scope.numPerPage;
              $scope.end = $scope.begin + $scope.numPerPage;
              if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
        
              $scope.lista_eventos = Evento.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'fecha_inicio DESC',
                        limit: $scope.numPerPage,
                        skip:  $scope.begin
                    }
              });
        }

        $scope.muestraResultadosBusqueda = function() {

              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {and: [
                      {campusId: $scope.currentUser.campus_pertenece_id},
                      {nombre: {
                          like: $scope.nombre_buscar+ '.*', 
                          options: 'i'
                      }}
              ]};

              Evento.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                    $scope.totalItems = resp.count;
                    if($scope.end > $scope.totalItems)
                        $scope.end = $scope.totalItems;
                    if($scope.totalItems == 0) {
                        $scope.begin = -1;
                        $scope.end = 0;              
                    }
              });                                  

              $scope.lista_eventos = Evento.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'fecha_inicio DESC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1
                    }
              });

              $scope.mostrarbtnLimpiar = true;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.limpiaBusqueda = function() {

              $scope.nombre_buscar = '';
              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id};

              Evento.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                  $scope.totalItems = resp.count;
                  if($scope.end > $scope.totalItems)
                      $scope.end = $scope.totalItems;
                  if($scope.totalItems == 0) {
                      $scope.begin = -1;
                      $scope.end = 0;              
                  }
              });                                  

              $scope.lista_eventos = Evento.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'fecha_inicio DESC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1
                    }
              });

              $scope.mostrarbtnLimpiar = false;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.trustSrc = function(src) {
          if(src === undefined)
            src = 'img/placeholder.png';
            return $sce.trustAsResourceUrl(src);
        }

        $scope.muestraDatosRegistroSeleccionado = function(RegistroSeleccionado) {
              var index = $scope.lista_eventos.indexOf(RegistroSeleccionado);
              $scope.DatosRegistroActual = $scope.lista_eventos[index];
              $scope.client = 2;
              $scope.selectedRow = index;
        };


        $scope.altaRegistro = function() {
              $state.go('index.alta_evento');
        };


        $scope.activarRegistro = function(RegistroSeleccionado) {

              $scope.idx_registro = $scope.lista_eventos.indexOf(RegistroSeleccionado);
              
              swal({
                title: "Activar evento",
                html: '¿Desea activar evento <strong>'+RegistroSeleccionado.nombre +'</strong> ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Activar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
              }, function(){
                    swal.disableButtons();

                    Evento.prototype$updateAttributes({ id: RegistroSeleccionado.id }, {activo: true})
                    .$promise
                    .then(function(cliente) {
                        $scope.lista_eventos[$scope.idx_registro].activo = true;
                        swal('Evento activado', '', 'success');
                    });
              });

        };


        $scope.desactivarRegistro = function(RegistroSeleccionado) {

              $scope.idx_registro = $scope.lista_eventos.indexOf(RegistroSeleccionado);

              swal({
                title: "Desactivar evento",
                html: '¿Desea desctivar el evento <strong>'+RegistroSeleccionado.nombre +'</strong> ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Desactivar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
              }, function(){
                    swal.disableButtons();
                    Evento.prototype$updateAttributes({ id: RegistroSeleccionado.id }, {activo: false})
                    .$promise
                    .then(function(evento) {
                        $scope.lista_eventos[$scope.idx_registro].activo = false;
                        swal('evento desactivado', '', 'success');                            
                    });
              });
        };


        $scope.eliminaEvento = function(RegistroSeleccionado) {

              $scope.idx_registro = $scope.lista_eventos.indexOf(RegistroSeleccionado);

              swal({
                title: "Eliminar evento",
                html: '¿Desea eliminar el evento <strong>'+RegistroSeleccionado.nombre +'</strong> ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
              }, function(){
                    swal.disableButtons();

                    Evento.deleteById({ id: RegistroSeleccionado.id })
                    .$promise
                    .then(function() {

                            $scope.client = 1;
                            $scope.selectedRow = undefined;

                            $scope.totalItems--;
                            $scope.currentPage = 1;
                            $scope.begin = 0;
                            $scope.end = 5;

                            if($scope.end > $scope.totalItems)
                                $scope.end = $scope.totalItems;
                            if($scope.totalItems == 0) {
                                $scope.begin = -1;
                                $scope.end = 0;              
                            }

                            $scope.lista_eventos = Evento.find({
                                  filter: {
                                      where: $scope.condicion,
                                      order: 'fecha_inicio DESC',
                                      limit: $scope.numPerPage,
                                      skip: $scope.currentPage - 1,
                                      fields:['id','nombre','contenido','img_principal','ubicacion','fecha_inicio','activo']
                                  }
                            });

                            swal('Evento eliminado', '', 'success');
                    });

              });

        };
};



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function AltaEventoController($scope, Evento, Programa, $sce, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Nuevo evento';
      $scope.txt_boton = 'Agregar';

      $scope.dateOptions = {
         dateFormat: "dd/mm/yy",
         regional: "es",
         changeMonth: true,
         changeYear: true,
         yearRange: "2015:2019"
      };

      $scope.temp = {
        img_principal: {},
        img_mapa_sitio: {}
      };

      $scope.video_preview = 'img/placeholder.png';

      $scope.evento = {
          nombre         : "",
          contenido      : "",
          fechas_evento  : "",
          ubicacion      : "",
          img_principal  : "img/placeholder.png",
          fecha_inicio   : "",
          fecha_cierre   : "",
          img_mapa_sitio : "img/placeholder.png",
          activo         : true
      };

      $scope.ngData = [];

      $scope.gridOptions = {};
      $scope.gridOptions.enableCellEditOnFocus = true;
      $scope.gridOptions.data = $scope.ngData;
      $scope.gridOptions.columnDefs = [
            {field: 'fecha', displayName: 'Fecha' , type: 'date', cellFilter: 'date:"dd/MM/yyyy"', enableSorting: false, enableHiding: false, width: 120},
            {field:'hora', displayName:'Hora', enableSorting: false, enableHiding: false, width: 120},
            {field:'actividad', displayName:'Actividad', enableSorting: false, enableHiding: false,},
            {field:'ponente', displayName:'Ponente', enableSorting: false, enableHiding: false, width: 220},
            {field: 'accion', displayName:'', enableSorting: false, enableHiding: false, width: 40, enableCellEdit: false, cellClass: 'grid-align', cellTemplate: '<button type="button" class="btn btn-xs btn-danger" ng-click="grid.appScope.eliminaTema(row.entity)" ><i class="fa fa-times"></i></button>'}
      ];


       $scope.agregar_tema = function() {
            if($scope.ngData.length > 0)
              $scope.ngData.push({fecha: $scope.ngData[$scope.ngData.length-1].fecha, hora: '', actividad: '', ponente:'', accion:''});
            else
              $scope.ngData.push({fecha: '', hora: '', actividad: '', ponente:'', accion:''});
       };

       $scope.eliminaTema = function(data) {
          var idx_registro = $scope.ngData.indexOf(data);
          $scope.ngData.splice(idx_registro, 1);
       }


      $scope.borraFoto1 = function () {
            $scope.evento.img_principal = 'img/placeholder.png';
      }

      $scope.borraFoto2 = function () {
            $scope.evento.img_mapa_sitio = 'img/placeholder.png';
      }

      $scope.verPreview = function() {
          $scope.video_preview = $scope.evento.url_video;
      };

      $scope.trustSrc = function(src) {
          return $sce.trustAsResourceUrl(src);
      }

      $scope.guardaDatos = function() {

            if($scope.evento.img_principal === 'img/placeholder.png')
              $scope.evento.img_principal = '';

            if($scope.temp.img_principal.base64 != undefined)
            {
                if($scope.evento.img_principal !== $scope.temp.img_principal.base64)
                  $scope.evento.img_principal = "data:"+$scope.temp.img_principal.filetype+";base64,"+$scope.temp.img_principal.base64;
            }

            if($scope.evento.img_mapa_sitio === 'img/placeholder.png')
              $scope.evento.img_mapa_sitio = '';

            if($scope.temp.img_mapa_sitio.base64 != undefined)
            {
                if($scope.evento.img_mapa_sitio !== $scope.temp.img_mapa_sitio.base64)
                  $scope.evento.img_mapa_sitio = "data:"+$scope.temp.img_mapa_sitio.filetype+";base64,"+$scope.temp.img_mapa_sitio.base64;
            }

            var fecha = new Date($scope.evento.fecha_inicio);
            fecha.setHours(00);
            fecha.setMinutes(00);
            fecha.setSeconds(00);
            fecha.setMilliseconds(00);
            $scope.evento.fecha_inicio = fecha;

            fecha = new Date($scope.evento.fecha_cierre);
            fecha.setHours(00);
            fecha.setMinutes(00);
            fecha.setSeconds(00);
            fecha.setMilliseconds(00);
            $scope.evento.fecha_cierre = fecha;

            Evento
              .create({
                  nombre         :$scope.evento.nombre,
                  descripcion    :$scope.evento.descripcion,
                  contenido      :$scope.evento.contenido,
                  fechas_evento  :$scope.evento.fechas_evento,
                  ubicacion      :$scope.evento.ubicacion,
                  img_principal  :$scope.evento.img_principal,
                  fecha_inicio   :$scope.evento.fecha_inicio,
                  fecha_cierre   :$scope.evento.fecha_cierre,
                  img_mapa_sitio :$scope.evento.img_mapa_sitio,
                  url_video      :$scope.evento.url_video,
                  activo         :$scope.evento.activo,
                  campusId       :$scope.currentUser.campus_pertenece_id
              })
              .$promise
              .then(function(nuevo_evento) {

                    for(var i=0; i < $scope.ngData.length; i++)
                    {
                            Programa
                            .create({
                                orden     :i,
                                fecha     :$scope.ngData[i].fecha,
                                hora      :$scope.ngData[i].hora,
                                actividad :$scope.ngData[i].actividad,
                                ponente   :$scope.ngData[i].ponente,
                                eventoId  :nuevo_evento.id
                            })
                            .$promise
                            .then(function(programa) {
                            });

                    }
                    $state.go('index.lista_eventos');
              });

      };

      $scope.cancelaForm = function() {
          $state.go('index.lista_eventos');
      };

};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function EditaEventoController($scope, $q, Evento, Programa, $sce, $stateParams, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Editar evento';
      $scope.txt_boton = 'Guardar';
      $scope.btn_buscar_cliente = false;

      $scope.dateOptions = {
         dateFormat: "dd/mm/yy",
         regional: "es",
         changeMonth: true,
         changeYear: true,
         yearRange: "2015:2019"
      };

      $scope.temp = {
        img_principal: {},
        img_mapa_sitio: {}
      };

      $scope.ngData = [];

      $scope.gridOptions = {};
      $scope.gridOptions.enableCellEditOnFocus = true;
      $scope.gridOptions.data = $scope.ngData;
      $scope.gridOptions.columnDefs = [
            {field: 'fecha', displayName: 'Fecha' , type: 'date', cellFilter: 'date:"dd/MM/yyyy"', enableSorting: false, enableHiding: false, width: 120},
            {field:'hora', displayName:'Hora', enableSorting: false, enableHiding: false, width: 120},
            {field:'actividad', displayName:'Actividad', enableSorting: false, enableHiding: false,},
            {field:'ponente', displayName:'Ponente', enableSorting: false, enableHiding: false, width: 220},
            {field: 'accion', displayName:'', enableSorting: false, enableHiding: false, width: 40, enableCellEdit: false, cellClass: 'grid-align', cellTemplate: '<button type="button" class="btn btn-xs btn-danger" ng-click="grid.appScope.eliminaTema(row.entity)" ><i class="fa fa-times"></i></button>'}
      ];


       $scope.agregar_tema = function() {
            if($scope.ngData.length > 0)
              $scope.ngData.push({fecha: $scope.ngData[$scope.ngData.length-1].fecha, hora: '', actividad: '', ponente:'', accion:''});
            else
              $scope.ngData.push({fecha: '', hora: '', actividad: '', ponente:'', accion:''});
       };

       $scope.eliminaTema = function(data) {
          var idx_registro = $scope.ngData.indexOf(data);
          $scope.ngData.splice(idx_registro, 1);
       }

      $scope.verPreview = function() {
          $scope.video_preview = $scope.evento.url_video;
      };

      $scope.trustSrc = function(src) {
          return $sce.trustAsResourceUrl(src);
      }


      $q
        .all([
          Evento.findById({ 
              id: $stateParams.id,
              filter: {
                include: [
                    {
                          relation: 'programa',
                          scope: {
                              order: 'orden ASC'
                          }
                    }
                ]
              }
          }).$promise
        ])
        .then(function(data) {
            $scope.evento = data[0];
            if($scope.evento.programa.length > 0) {
              $scope.ngData = $scope.evento.programa;
              $scope.gridOptions.data = $scope.ngData;
            }
      
            if($scope.evento.url_video === undefined)
              $scope.evento.url_video = '';

            if($scope.evento.img_principal === '' || $scope.evento.img_principal === undefined)
              $scope.evento.img_principal = 'img/placeholder.png';
            
            if($scope.evento.img_mapa_sitio === '' || $scope.evento.img_mapa_sitio === undefined)
              $scope.evento.img_mapa_sitio = 'img/placeholder.png';
        });


      $scope.borraFoto1 = function () {
            $scope.evento.img_principal = 'img/placeholder.png';
      }

      $scope.borraFoto2 = function () {
            $scope.evento.img_mapa_sitio = 'img/placeholder.png';
      }


      $scope.guardaDatos = function() {

            if($scope.evento.img_principal === 'img/placeholder.png')
              $scope.evento.img_principal = '';

            if($scope.temp.img_principal.base64 != undefined)
            {
                if($scope.evento.img_principal !== $scope.temp.img_principal.base64)
                  $scope.evento.img_principal = "data:"+$scope.temp.img_principal.filetype+";base64,"+$scope.temp.img_principal.base64;
            }
            
            if($scope.evento.img_mapa_sitio === 'img/placeholder.png')
              $scope.evento.img_mapa_sitio = '';

            if($scope.temp.img_mapa_sitio.base64 != undefined)
            {
                if($scope.evento.img_mapa_sitio !== $scope.temp.img_mapa_sitio.base64)
                  $scope.evento.img_mapa_sitio = "data:"+$scope.temp.img_mapa_sitio.filetype+";base64,"+$scope.temp.img_mapa_sitio.base64;
            }
            

            var fecha = new Date($scope.evento.fecha_inicio);
            fecha.setHours(00);
            fecha.setMinutes(00);
            fecha.setSeconds(00);
            fecha.setMilliseconds(00);
            $scope.evento.fecha_inicio = fecha;

            fecha = new Date($scope.evento.fecha_cierre);
            fecha.setHours(00);
            fecha.setMinutes(00);
            fecha.setSeconds(00);
            fecha.setMilliseconds(00);
            $scope.evento.fecha_cierre = fecha;

            Evento.prototype$updateAttributes({ id: $scope.evento.id }, $scope.evento)
            .$promise
            .then(function(cliente) {

                  Evento.programa.destroyAll({ id: $scope.evento.id })
                    .$promise
                    .then(function() {

                          for(var i=0; i < $scope.ngData.length; i++)
                          {
                                  Programa
                                  .create({
                                      orden     :i,
                                      fecha     :$scope.ngData[i].fecha,
                                      hora      :$scope.ngData[i].hora,
                                      actividad :$scope.ngData[i].actividad,
                                      ponente   :$scope.ngData[i].ponente,
                                      eventoId  :$scope.evento.id
                                  })
                                  .$promise
                                  .then(function(programa) {
                                  });

                          }
                          
                         $state.go('index.lista_eventos'); 
                  });
                  
            })
            .catch(function(error) {
              if(error.status == 413)
                alert("El tamaño del archivo de imagen es muy grande");
            });

      };

      $scope.cancelaForm = function() {
          $state.go('index.lista_eventos');
      };

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

angular
    .module('inspinia')
    .controller('ListaEventosController' , ['$scope', 'Evento', 'SweetAlert', '$sce', '$state', '$stateParams', ListaEventosController])
    .controller('AltaEventoController'   , ['$scope', 'Evento', 'Programa', '$sce', '$state', AltaEventoController])
    .controller('EditaEventoController'  , ['$scope', '$q', 'Evento', 'Programa', '$sce', '$stateParams', '$state',  EditaEventoController])