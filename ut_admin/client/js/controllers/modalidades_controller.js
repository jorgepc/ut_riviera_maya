function ListaModalidadesController($scope, $q, Modalidad, $state, $stateParams, $modal) {

        $scope.$parent.$parent.main.titulo_seccion = 'Modalidades';
        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.begin = 0;
        $scope.end = 5;
        $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id };

        Modalidad
        .count({
          where: $scope.condicion
        })
        .$promise
        .then(function(resp) {
            $scope.totalItems = resp.count;
            if($scope.end > $scope.totalItems)
              $scope.end = $scope.totalItems;
        });                                  

        $scope.modalidades = Modalidad.find({
              filter: {
                where: $scope.condicion,
                order: 'nombre ASC',
                limit: $scope.numPerPage,
                skip: $scope.currentPage - 1
              }
        });

        
        $scope.pageChanged = function () {

              $scope.begin = ($scope.currentPage - 1) * $scope.numPerPage;
              $scope.end = $scope.begin + $scope.numPerPage;
              if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
        
              $scope.modalidades = Modalidad.find({
                    filter: {
                      where: $scope.condicion,
                      order: 'nombre ASC',
                      limit: $scope.numPerPage,
                      skip:  $scope.begin     
                    }
              });
        }

        $scope.muestraResultadosBusqueda = function() {

            $scope.currentPage = 1;
            $scope.begin = 0;
            $scope.end = 5;
            $scope.condicion = {and: [
                    {campusId: $scope.currentUser.campus_pertenece_id},
                    {nombre: {
                        like: $scope.nombre_buscar+ '.*', 
                        options: 'i'
                    }}
            ]};

            Modalidad
            .count({
                where: $scope.condicion
            })
            .$promise
            .then(function(resp) {
                $scope.totalItems = resp.count;

                if($scope.totalItems < $scope.end)
                {
                    $scope.end = $scope.totalItems;              
                }

                if($scope.totalItems == 0)
                {
                    $scope.begin = -1;
                    $scope.end = 0;              
                }
            });                                  

            $scope.modalidades = Modalidad.find({
                  filter: {
                    where: $scope.condicion,
                    order: 'nombre ASC',
                    limit: $scope.numPerPage,
                    skip: $scope.currentPage - 1
                  }
            });

            $scope.mostrarbtnLimpiar = true;
        };

        $scope.limpiaBusqueda = function() {

            $scope.nombre_buscar = '';
            $scope.currentPage = 1;
            $scope.numPerPage = 5;
            $scope.begin = 0;
            $scope.end = 5;
            $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id};

            Modalidad
            .count({
              where: $scope.condicion
            })
            .$promise
            .then(function(resp) {
              $scope.totalItems = resp.count;
            });                                  

            $scope.modalidades = Modalidad.find({
                  filter: {
                    where: $scope.condicion,
                    order: 'nombre ASC',
                    limit: $scope.numPerPage,
                    skip: $scope.currentPage - 1
                  }
            });

            $scope.mostrarbtnLimpiar = false;
        };



        $scope.modificarModalidad = function(DatosModalidadActual) {

              $scope.idx_registro = $scope.modalidades.indexOf(DatosModalidadActual);
      
              swal({
                title: 'Editar',   
                html: '<p><input id="input-field" class="form-control" value="'+DatosModalidadActual.nombre+'">',
                showCancelButton: true,   
                closeOnConfirm: true
              }, 
              function() {   
                    Modalidad.prototype$updateAttributes(
                      { id: DatosModalidadActual.id }, 
                      {
                        nombre: $('#input-field').val()
                    })
                    .$promise.then(function() {
                          $scope.modalidades[$scope.idx_registro].nombre = $('#input-field').val();
                    });
              });

        };

        $scope.altaModalidad = function() {

              swal({
                title: 'Nueva modalidad',   
                html: '<p><input id="input-field" class="form-control">',
                showCancelButton: true,   
                closeOnConfirm: true
              }, 
              function() {   
                  Modalidad
                    .create({
                        nombre   :$('#input-field').val(),
                        campusId :$scope.currentUser.campus_pertenece_id
                    })
                    .$promise
                    .then(function(nuevo_grupo) {
          
                        $scope.totalItems++;
                        $scope.currentPage = 1;
                        $scope.begin = 0;
                        $scope.end = 5;

                        if($scope.end > $scope.totalItems)
                          $scope.end = $scope.totalItems;

                          $scope.modalidades = Modalidad.find({
                                filter: {
                                  where: $scope.condicion,
                                  order: 'nombre ASC',
                                  limit: $scope.numPerPage,
                                  skip: $scope.currentPage - 1
                                }
                          });
                    });                                  
              });

        };


      	$scope.eliminaModalidad = function(DatosModalidadActual) {

              $scope.idx_eliminar = $scope.modalidades.indexOf(DatosModalidadActual);

              Modalidad.RegistroCarreras.count({ id: DatosModalidadActual.id })
                .$promise
                .then(function(resultado) {
                  
                      if(resultado.count > 0)
                      {
                            
                              swal({
                                title: "Eliminar modalidad",
                                html: 'La modalidad <strong>'+DatosModalidadActual.nombre +'</strong> no se puede eliminar ya que tiene carreras registradas</p>',
                                type: "warning",
                                showCConfirmButton: true,
                                showCancelButton: false,
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "Aceptar",
                                closeOnConfirm: true
                              });                            
                      }
                      else
                      {
                              swal({
                                title: "Eliminar modalidad",
                                html: '¿Desea eliminar la modalidad <strong>'+DatosModalidadActual.nombre +'</strong> ?',
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "Eliminar",
                                cancelButtonText: "Cancelar",
                                closeOnConfirm: false,
                                closeOnCancel: true
                              }, function(){
                                    swal.disableButtons();

                                    Modalidad.deleteById({ id: DatosModalidadActual.id })
                                    .$promise
                                    .then(function() {
                                            $scope.totalItems--;
                                            $scope.currentPage = 1;
                                            $scope.begin = 0;
                                            $scope.end = 5;
                                            $scope.mostrarbtnLimpiar = false;
                                            $scope.nombre_buscar = '';

                                            if($scope.end > $scope.totalItems)
                                              $scope.end = $scope.totalItems;

                                              $scope.modalidades = Modalidad.find({
                                                    filter: {
                                                      where: $scope.condicion,
                                                      order: 'nombre ASC',
                                                      limit: $scope.numPerPage,
                                                      skip: $scope.currentPage - 1
                                                    }
                                              });
                                            swal('Modalidad eliminada', '', 'success');
                                    });
                              });
                      }
              });

      	};

};


angular
    .module('inspinia')
    .controller('ListaModalidadesController' , ['$scope', '$q', 'Modalidad', '$state', '$stateParams', '$modal', ListaModalidadesController])
