/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */

/**
 * MainCtrl - controller
 */
function MainCtrl() {
};

angular
    .module('inspinia')
	.controller('MainCtrl', MainCtrl);