function ListaCarrerasController($scope, Carrera, $state, $stateParams, $modal) {

        $scope.$parent.$parent.main.titulo_seccion = 'Carreras';
        $scope.mostrarbtnLimpiar = false;
        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.begin = 0;
        $scope.end = 5;
        $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id };

        Carrera.count({
          where: $scope.condicion
        })
        .$promise
        .then(function(resp) {
          $scope.totalItems = resp.count;
          if($scope.end > $scope.totalItems)
            $scope.end = $scope.totalItems;
        });                                  

        $scope.carreras = Carrera.find({
              filter: {
                  where: $scope.condicion,
                  order: 'nombre ASC',
                  limit: $scope.numPerPage,
                  skip: $scope.currentPage - 1,
                  include: [
                    'modalidad_pertenece'
                  ]
              }
        });

        $scope.pageChanged = function () {

              $scope.begin = ($scope.currentPage - 1) * $scope.numPerPage;
              $scope.end = $scope.begin + $scope.numPerPage;
              if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
        
              $scope.carreras = Carrera.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'nombre ASC',
                        limit: $scope.numPerPage,
                        skip:  $scope.begin,          
                        include: [
                          'modalidad_pertenece'
                        ]
                    }
              });
        }

        $scope.muestraResultadosBusqueda = function() {

            $scope.currentPage = 1;
            $scope.begin = 0;
            $scope.end = 5;
            $scope.condicion = {and: [
                    {campusId: $scope.currentUser.campus_pertenece_id},
                    {nombre: {
                        like: $scope.nombre_buscar+ '.*', 
                        options: 'i'
                    }}
            ]};

            Carrera.count({
                where: $scope.condicion
            })
            .$promise
            .then(function(resp) {
                $scope.totalItems = resp.count;
                if($scope.totalItems < $scope.end)
                    $scope.end = $scope.totalItems;              

                if($scope.totalItems == 0)
                {
                    $scope.begin = -1;
                    $scope.end = 0;              
                }
            });                                  

            $scope.carreras = Carrera.find({
                  filter: {
                      where: $scope.condicion,
                      order: 'nombre ASC',
                      limit: $scope.numPerPage,
                      skip: $scope.currentPage - 1,
                      include: [
                        'modalidad_pertenece'
                      ]
                  }
            });

            $scope.mostrarbtnLimpiar = true;
            $scope.client = 1;
            $scope.selectedRow = undefined;
};

        $scope.limpiaBusqueda = function() {

            $scope.nombre_buscar = '';
            $scope.currentPage = 1;
            $scope.begin = 0;
            $scope.end = 5;
            $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id};

            Carrera.count({
              where: $scope.condicion
            })
            .$promise
            .then(function(resp) {
              $scope.totalItems = resp.count;
            });                                  

            $scope.carreras = Carrera.find({
                  filter: {
                      where: $scope.condicion,
                      order: 'nombre ASC',
                      limit: $scope.numPerPage,
                      skip: $scope.currentPage - 1,          
                      include: [
                        'modalidad_pertenece'
                      ]
                  }
            });

            $scope.mostrarbtnLimpiar = false;
            $scope.client = 1;
            $scope.selectedRow = undefined;
        };


      	$scope.muestraDatosCarreraActual = function(carreraseleccionada) {
              var index = $scope.carreras.indexOf(carreraseleccionada);
          		$scope.DatosCarreraActual = $scope.carreras[index];
          		$scope.client = 2;
              $scope.selectedRow = index;
      	};

        $scope.altaCarrera = function() {
          $state.go('oferta_ed.alta_carrera');
        };

      	$scope.elimina_carrera = function(carreraseleccionada) {

            $scope.idx_eliminar = $scope.carreras.indexOf(carreraseleccionada);

            swal({
              title: "Eliminar carrera",
              html: '¿Desea eliminar la carrera <strong>'+carreraseleccionada.nombre +'</strong> ?',
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              confirmButtonText: "Eliminar",
              cancelButtonText: "Cancelar",
              closeOnConfirm: false,
              closeOnCancel: true
            }, function(){
                  swal.disableButtons();

                  Carrera.deleteById({ id: carreraseleccionada.id })
                  .$promise
                  .then(function() {
                          $scope.client = 1;
                          $scope.selectedRow = undefined;

                          $scope.totalItems--;
                          $scope.currentPage = 1;
                          $scope.begin = 0;
                          $scope.end = 5;
                          $scope.mostrarbtnLimpiar = false;
                          $scope.nombre_buscar = '';

                          if($scope.end > $scope.totalItems)
                            $scope.end = $scope.totalItems;

                          $scope.carreras = Carrera.find({
                                filter: {
                                    where: $scope.condicion,
                                    order: 'nombre ASC',
                                    limit: $scope.numPerPage,
                                    skip: $scope.currentPage - 1,
                                    include: [
                                      'modalidad_pertenece'
                                    ]
                                }
                          });
                          swal('Carrera eliminada', '', 'success');
                  });
            });
      	};
};




function AltaCarreraController($scope, Carrera, Modalidad, $state, $timeout) {

      $scope.$parent.$parent.main.titulo_seccion = 'Nuevo Registro';
      $scope.txt_boton = 'Agregar';

      $scope.modalidades = Modalidad.find({
            filter: {
              where: {campusId: $scope.currentUser.campus_pertenece_id},
              order: 'nombre ASC'
            }
      });

      $scope.temp = {
        icono: {},
        img_principal: {}
      };

      $scope.carrera = {
        nombre        :"",
        icono         :"img/placeholder.png",
        img_principal :"img/placeholder.png",
        contenido     :"",
        modalidadId   :""
      };

      $scope.borraFoto = function () {
            $scope.carrera.icono = 'img/placeholder.png';
      }

      $scope.borraFotoPrincipal = function () {
            $scope.carrera.img_principal = 'img/placeholder.png';
      }

      $scope.guardaDatos = function() {

          if($scope.carrera.icono === 'img/placeholder.png')
            $scope.carrera.icono = '';

          if($scope.temp.icono != undefined)
          {
              if($scope.carrera.icono !== $scope.temp.icono.base64)
                $scope.carrera.icono = "data:"+$scope.temp.icono.filetype+";base64,"+$scope.temp.icono.base64;
          }

          if($scope.carrera.img_principal === 'img/placeholder.png')
            $scope.carrera.img_principal = '';

          if($scope.temp.img_principal != undefined)
          {
              if($scope.carrera.img_principal !== $scope.temp.img_principal.base64)
                $scope.carrera.img_principal = "data:"+$scope.temp.img_principal.filetype+";base64,"+$scope.temp.img_principal.base64;
          }

          Carrera
            .create({
                nombre        :$scope.carrera.nombre,
                icono         :$scope.carrera.icono,
                img_principal :$scope.carrera.img_principal,
                contenido     :$scope.carrera.contenido,
                modalidadId   :$scope.carrera.modalidadId,
                campusId      :$scope.currentUser.campus_pertenece_id
            })
            .$promise
            .then(function() {
              $state.go('oferta_ed.carreras');
            });
      };

      $scope.cancelaForm = function() {
          $state.go('oferta_ed.carreras');
      };

};


function EditaCarreraController($scope, $q, Carrera, Modalidad, $stateParams, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Editar Registro';
      $scope.txt_boton = 'Guardar';

      $scope.temp = {
        icono: {},
        img_principal: {}
      };


      $q
        .all([
          Carrera.findById({ 
              id: $stateParams.id
          }).$promise
        ])
        .then(function(data) {
                $scope.carrera = data[0];

                if($scope.carrera.icono === '' || $scope.carrera.icono === undefined)
                  $scope.carrera.icono = 'img/placeholder.png';

                if($scope.carrera.img_principal === '' || $scope.carrera.img_principal === undefined)
                  $scope.carrera.img_principal = 'img/placeholder.png';

                Modalidad.find({
                    filter: {
                      where: {campusId: $scope.currentUser.campus_pertenece_id},
                      order: 'nombre ASC'
                    }
                }) 
                .$promise
                .then(function(modalidades) {
                    $scope.modalidades = modalidades;
                    $scope.carrera = data[0];
                });
        });


      $scope.borraFoto = function () {
            $scope.carrera.icono = 'img/placeholder.png';
      }

      $scope.borraFotoPrincipal = function () {
            $scope.carrera.img_principal = 'img/placeholder.png';
      }

      $scope.guardaDatos = function() {

          if($scope.carrera.icono === 'img/placeholder.png')
            $scope.carrera.icono = '';

          if($scope.temp.icono.base64 != undefined)
          {
              if($scope.carrera.icono !== $scope.temp.icono.base64)
                $scope.carrera.icono = "data:"+$scope.temp.icono.filetype+";base64,"+$scope.temp.icono.base64;
          }

          if($scope.carrera.img_principal === 'img/placeholder.png')
            $scope.carrera.img_principal = '';

          if($scope.temp.img_principal.base64 != undefined)
          {
              if($scope.carrera.img_principal !== $scope.temp.img_principal.base64)
                $scope.carrera.img_principal = "data:"+$scope.temp.img_principal.filetype+";base64,"+$scope.temp.img_principal.base64;
          }

          $scope.carrera
            .$save()
            .then(function(carrera) {
              $state.go('oferta_ed.carreras');
            });
      };

      $scope.cancelaForm = function() {
            $state.go('oferta_ed.carreras');
      };

};



angular
    .module('inspinia')
    .controller('ListaCarrerasController' , ['$scope', 'Carrera', '$state', '$stateParams', '$modal', ListaCarrerasController])
    .controller('AltaCarreraController'   , ['$scope', 'Carrera', 'Modalidad', '$state', '$timeout', AltaCarreraController])
    .controller('EditaCarreraController'  , ['$scope', '$q', 'Carrera', 'Modalidad', '$stateParams', '$state',  EditaCarreraController])
