function AuthLoginController($scope, $timeout, AuthService, $state) {

    $scope.user = {
      username: 'adminapp',
      password: 'adminapp'
    };
    $scope.msg_error_login = false;

    $scope.login = function() {
      $scope.msg_error_login = false;
      AuthService.login($scope.user.username, $scope.user.password)
        .then(function() {
          if($scope.currentUser != undefined)
          {
              if($scope.currentUser.estatus == 200)
              {
                  $state.go('index.lista_eventos');
              }
              else if($scope.currentUser.estatus == 401)
              {
                  $scope.msg_error_login = true;
                  $timeout(function(){
                       $scope.msg_error_login = false;
                  }, 3000);
              }
          }

        });
    };

};

function AuthLogoutController($scope, AuthService, $state) {

    AuthService.logout()
      .then(function() {
        $state.go('login');
    });

};

function SignUpController($scope, AuthService, $state) {

    $scope.user = {
      email: 'baz@qux.com',
      password: 'bazqux'
    };

    $scope.register = function() {
      AuthService.register($scope.user.email, $scope.user.password)
        .then(function() {
          $state.transitionTo('sign-up-success');
        });
    };

};


angular
    .module('inspinia')
    .controller('AuthLoginController', ['$scope', '$timeout', 'AuthService', '$state', AuthLoginController])
    .controller('AuthLogoutController' , AuthLogoutController)
    .controller('SignUpController'     , SignUpController);
