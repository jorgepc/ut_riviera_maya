function ListaAlumnosCargasController($scope, Alumno, SweetAlert, $state, $stateParams) {

        $scope.$parent.$parent.main.titulo_seccion = 'Carga de datos de los alumnos';

        $scope.mostrarbtnLimpiar = false;

        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.begin = 0;
        $scope.end = 5;
        $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id };

        Alumno.count({
            where: $scope.condicion
        })
        .$promise
        .then(function(resp) {
            $scope.totalItems = resp.count;
            if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
            if($scope.totalItems == 0)
            {
                $scope.begin = -1;
                $scope.end = 0;
                $scope.inactivos = 0;
                $scope.activos = 0;
            }
        });


        $scope.lista_alumnos = Alumno.find({
              filter: {
                  where: $scope.condicion,
                  order: 'nombre_completo ASC',
                  limit: $scope.numPerPage,
                  skip: $scope.currentPage - 1,
                  include: [
                      'cargaAcademica'
                  ]
              }
        });


        $scope.pageChanged = function () {

              $scope.begin = ($scope.currentPage - 1) * $scope.numPerPage;
              $scope.end = $scope.begin + $scope.numPerPage;
              if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
        
              $scope.lista_alumnos = Alumno.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'nombre_completo ASC',
                        limit: $scope.numPerPage,
                        skip:  $scope.begin,
                        include: [
                            'cargaAcademica'
                        ]
                    }
              });
        }

        $scope.muestraResultadosBusqueda = function() {

              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {and: [
                      {campusId: $scope.currentUser.campus_pertenece_id},
                      {nombre_completo: {
                          like: $scope.nombre_buscar+ '.*', 
                          options: 'i'
                      }}
              ]};

              Alumno.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                    $scope.totalItems = resp.count;
                    if($scope.end > $scope.totalItems)
                        $scope.end = $scope.totalItems;
                    if($scope.totalItems == 0) {
                        $scope.begin = -1;
                        $scope.end = 0;              
                    }
              });                                  

              $scope.lista_alumnos = Alumno.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'nombre_completo ASC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1,
                        include: [
                            'cargaAcademica'
                        ]
                    }
              });

              $scope.mostrarbtnLimpiar = true;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.limpiaBusqueda = function() {

              $scope.nombre_buscar = '';
              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id};

              Alumno.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                  $scope.totalItems = resp.count;
                  if($scope.end > $scope.totalItems)
                      $scope.end = $scope.totalItems;
                  if($scope.totalItems == 0) {
                      $scope.begin = -1;
                      $scope.end = 0;              
                  }
              });                                  

              $scope.lista_alumnos = Alumno.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'nombre_completo ASC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1,
                        include: [
                            'cargaAcademica'
                        ]
                    }
              });

              $scope.mostrarbtnLimpiar = false;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.muestraDatosRegistroSeleccionado = function(RegistroSeleccionado) {
              var index = $scope.lista_alumnos.indexOf(RegistroSeleccionado);
              $scope.DatosAlumnoActual = $scope.lista_alumnos[index];
              $scope.client = 2;
              $scope.selectedRow = index;
        };


        $scope.importarCarga = function() {
              $state.go('index.importar_carga');
        };

};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ImportaCargaController($scope, Alumno, Campus, Carga_academica, SweetAlert, $filter, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Importar carga académica';
      $scope.porcentaje_avance = 0;

      $scope.gridOptions = {};
      $scope.gridOptions.data = [];
      $scope.gridOptions.columnDefs = [
            {field:'clave_alumno' , displayName:'Clave alumno', enableSorting: false, enableHiding: false, width: 120},
            {field:'nombres'      , displayName:'Nombres', enableSorting: false, enableHiding: false, width: 260},
            {field:'apellidos'    , displayName:'Apellidos', enableSorting: false, enableHiding: false, width: 260},
            {field:'carrera'      , displayName:'Carrera', enableSorting: false, enableHiding: false, width: 260},
            {field:'semestre'     , displayName:'Semestre', enableSorting: false, enableHiding: false, width: 260},
            
            {field:'materia'      , displayName:'Materia', enableSorting: false, enableHiding: false, width: 390},
            {field:'lunes'        , displayName:'Lunes', enableSorting: false, enableHiding: false, width: 120},
            {field:'martes'       , displayName:'Martes', enableSorting: false, enableHiding: false, width: 120},
            {field:'miercoles'    , displayName:'Miércoles', enableSorting: false, enableHiding: false, width: 120},
            {field:'jueves'       , displayName:'Jueves', enableSorting: false, enableHiding: false, width: 120},
            {field:'viernes'      , displayName:'Viernes', enableSorting: false, enableHiding: false, width: 120},
            {field:'sabado'       , displayName:'Sabado', enableSorting: false, enableHiding: false, width: 120}
      ];


      $scope.resetTabla = function() {
          $scope.gridOptions.data = [];
          //$scope.gridOptions.columnDefs = [];
      };

      $scope.guardaDatos = function() {

          swal({
            title: "Importar",
            html: '¿Desea realizar la importación de los alumnos y las cargas académicas ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true,
            closeOnCancel: true
          }, function(){
                //swal.disableButtons();

                //Primero ordenamos la info por la clave de alumno
                var datos = $filter('orderBy')($scope.gridOptions.data, 'clave_alumno', false);
                $scope.cont = 0;

                /*Campus.cargas_academicas_temp.destroyAll({ id: $scope.currentUser.campus_pertenece_id })
                .$promise
                .then(function() {

                      var intervalo;
                      var indice = 0;
                      var i = 0;
                      intervalo = setInterval(function(){ 
                          i = 0;
                          var objeto_alumnos = new Array;
                          while( (i < 50) && (indice < datos.length) )
                          {
                                var registro = datos[indice];                            
                                objeto_alumnos.push({
                                      clave_alumno : registro.clave_alumno,
                                      nombres      : registro.nombres,
                                      apellidos    : registro.apellidos,
                                      carrera      : registro.carrera,
                                      semestre     : registro.semestre,
                                      materia      : registro.materia,
                                      lunes        : (registro.lunes     == undefined ? '' : registro.lunes),
                                      martes       : (registro.martes    == undefined ? '' : registro.martes),
                                      miercoles    : (registro.miercoles == undefined ? '' : registro.miercoles),
                                      jueves       : (registro.jueves    == undefined ? '' : registro.jueves),
                                      viernes      : (registro.viernes   == undefined ? '' : registro.viernes),
                                      sabado       : (registro.sabado    == undefined ? '' : registro.sabado),
                                      campusId     : $scope.currentUser.campus_pertenece_id
                                });

                                i++;
                                indice++;
                                $scope.cont++;
                          }

                                Campus.cargas_academicas_temp.createMany(
                                  {id: $scope.currentUser.campus_pertenece_id},
                                  objeto_alumnos
                                )
                                .$promise
                                .then(function(nuevo_alumno) {
                                });

                                $scope.porcentaje_avance = $filter('number')( ( ($scope.cont / datos.length) * 100 ), 0);

                                if(indice >= datos.length)
                                {
                                  clearInterval(intervalo);
                                  $scope.porcentaje_avance = 0;
                                  swal({
                                    title: "Importación completa",
                                    html: '',
                                    type: 'success'
                                  }, function(){
                                      
                                      //$state.go('index.carga_academica');
                                  });
                                }
                      }, 1000);

                });*/


                //Borramos todos los alumnos del campus seleccionado
                Campus.alumnos.destroyAll({ id: $scope.currentUser.campus_pertenece_id })
                .$promise
                .then(function() { 
                        
                      //Borramos todas las cargas academicas del campus seleccionado
                      Campus.cargas_academicas.destroyAll({ id: $scope.currentUser.campus_pertenece_id })
                      .$promise
                      .then(function() {

                              //Creamos el array objeto_alumnos y el array vector_materias los cuales contendran la lista de alumnos y la lista de materias. en el vector_materias el id sera la clave del alumno
                              var objeto_alumnos = new Array;
                              var objeto_carga = new Array;
                              var vector_materias = new Array;
                              var clave_anterior = '';

                              objeto_alumnos.push({
                                clave_alumno    : datos[0].clave_alumno,
                                nombres         : datos[0].nombres,
                                apellidos       : datos[0].apellidos,
                                carrera         : datos[0].carrera,
                                semestre        : datos[0].semestre
                              });

                              clave_anterior = datos[0].clave_alumno;

                              //Aqui se llenan los dos array de donde se obtendran los datos para la bd
                              angular.forEach(datos, function(registro) {

                                  if(clave_anterior != registro.clave_alumno)
                                  {
                                      clave_anterior = registro.clave_alumno;

                                      //la llave para encontrar las materias del alumnos dentro del vector "vector_materias" sera el campo clave_alumno
                                      vector_materias[objeto_alumnos[objeto_alumnos.length - 1].clave_alumno] = objeto_carga;
                                      objeto_carga = new Array;

                                      objeto_alumnos.push({
                                        clave_alumno    : registro.clave_alumno,
                                        nombres         : registro.nombres,
                                        apellidos       : registro.apellidos,
                                        carrera         : registro.carrera,
                                        semestre        : registro.semestre
                                      });
                                  }
                                      objeto_carga.push({
                                        materia      : registro.materia,
                                        lunes        : registro.lunes,
                                        martes       : registro.martes,
                                        miercoles    : registro.miercoles,
                                        jueves       : registro.jueves,
                                        viernes      : registro.viernes,
                                        sabado       : registro.sabado
                                      });

                              });

                              vector_materias[objeto_alumnos[objeto_alumnos.length - 1].clave_alumno] = objeto_carga;

                              objeto_carga = undefined;                              

                              var intervalo;
                              var indice = 0;
                              var i = 0;

                              //Metemos el prceso de actualizacion al servidor dentro de un intervalo para no bloquear el navegador y no saturar el servidor
                              intervalo = setInterval(function(param_objeto_alumnos)
                              { 
                                        var array_objeto_alumnos = param_objeto_alumnos[0];
                                        i = 0;
                                        var array_alumnos_enviar = new Array;

                                        //Se creara un vector termporal que contenga los datos de 50 alumnos del vector array_objeto_alumnos para enviar al servidor
                                        while( (i < 50) && (indice < array_objeto_alumnos.length) )
                                        {
                                              var registro = array_objeto_alumnos[indice];

                                              array_alumnos_enviar.push({
                                                    clave_alumno    : registro.clave_alumno,
                                                    nombre_completo : (registro.apellidos + ' ' + registro.nombres),
                                                    nombres         : registro.nombres,
                                                    apellidos       : registro.apellidos,
                                                    carrera         : registro.carrera,
                                                    semestre        : registro.semestre,
                                                    campusId        : $scope.currentUser.campus_pertenece_id
                                              });

                                              i++;
                                              indice++;
                                        }

                                        Campus.alumnos.createMany(
                                          {id: $scope.currentUser.campus_pertenece_id},
                                          array_alumnos_enviar
                                        )
                                        .$promise
                                        .then(function(nuevos_alumnos) {

                                                //Al regresar el dato del servidor de cuales alumnos ya se agregaron se procede a armar el array de materias ya con el dato del id del alumno para enviar al servidor
                                                angular.forEach(nuevos_alumnos, function(alumno) {

                                                        var lista_materias = vector_materias[alumno.clave_alumno];
                                                        var lista_materias_enviar = new Array;
                                                        angular.forEach(lista_materias, function(fila) {

                                                              lista_materias_enviar.push({
                                                                  campusId     : $scope.currentUser.campus_pertenece_id,
                                                                  alumnoId     : alumno.id,
                                                                  materia      : fila.materia,
                                                                  lunes        : (fila.lunes     == undefined ? '' : fila.lunes),
                                                                  martes       : (fila.martes    == undefined ? '' : fila.martes),
                                                                  miercoles    : (fila.miercoles == undefined ? '' : fila.miercoles),
                                                                  jueves       : (fila.jueves    == undefined ? '' : fila.jueves),
                                                                  viernes      : (fila.viernes   == undefined ? '' : fila.viernes),
                                                                  sabado       : (fila.sabado    == undefined ? '' : fila.sabado)
                                                              })

                                                        });

                                                        Alumno.cargaAcademica.createMany(
                                                          {id: alumno.id},
                                                          lista_materias_enviar
                                                        )
                                                        .$promise
                                                        .then(function(nuevo_evento) {
                                                        });

                                                });

                                        });

                                        $scope.porcentaje_avance = $filter('number')( ( (indice / array_objeto_alumnos.length) * 100 ), 0);

                                        if(indice >= array_objeto_alumnos.length)
                                        {
                                          clearInterval(intervalo);
                                          $scope.porcentaje_avance = 100;
                                          swal({
                                            title: "Importación completa",
                                            html: '',
                                            type: 'success'
                                          }, function(){
                                              $scope.porcentaje_avance = 0;
                                               objeto_alumnos = undefined;
                                              //$state.go('index.carga_academica');
                                          });
                                        }
                              }, 3000, [objeto_alumnos]);

                      });

                });
                
          });


      };

      $scope.cancelaForm = function() {
          $state.go('index.carga_academica');
      };

};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


angular
    .module('inspinia')
    .controller('ListaAlumnosCargasController' , ['$scope', 'Alumno', 'SweetAlert', '$state', '$stateParams', ListaAlumnosCargasController])
    .controller('ImportaCargaController'   , ['$scope', 'Alumno', 'Campus', 'Carga_academica', 'SweetAlert', '$filter', '$state', ImportaCargaController])
