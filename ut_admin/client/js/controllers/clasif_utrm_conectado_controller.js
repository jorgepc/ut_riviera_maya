function ListaClasifUTRMController($scope, $q, ClasifVideosUTRM, $state, $stateParams, $modal) {

        $scope.$parent.$parent.main.titulo_seccion = 'Temas';
        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.begin = 0;
        $scope.end = 5;
        $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id };

        ClasifVideosUTRM
        .count({
          where: $scope.condicion
        })
        .$promise
        .then(function(resp) {
            $scope.totalItems = resp.count;
            if($scope.end > $scope.totalItems)
              $scope.end = $scope.totalItems;
        });                                  

        $scope.clasificaciones = ClasifVideosUTRM.find({
              filter: {
                where: $scope.condicion,
                order: 'nombre ASC',
                limit: $scope.numPerPage,
                skip: $scope.currentPage - 1
              }
        });

        
        $scope.pageChanged = function () {

              $scope.begin = ($scope.currentPage - 1) * $scope.numPerPage;
              $scope.end = $scope.begin + $scope.numPerPage;
              if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
        
              $scope.clasificaciones = ClasifVideosUTRM.find({
                    filter: {
                      where: $scope.condicion,
                      order: 'nombre ASC',
                      limit: $scope.numPerPage,
                      skip:  $scope.begin     
                    }
              });
        }

        $scope.muestraResultadosBusqueda = function() {

            $scope.currentPage = 1;
            $scope.begin = 0;
            $scope.end = 5;
            $scope.condicion = {and: [
                    {campusId: $scope.currentUser.campus_pertenece_id},
                    {nombre: {
                        like: $scope.nombre_buscar+ '.*', 
                        options: 'i'
                    }}
            ]};

            ClasifVideosUTRM
            .count({
                where: $scope.condicion
            })
            .$promise
            .then(function(resp) {
                $scope.totalItems = resp.count;

                if($scope.totalItems < $scope.end)
                {
                    $scope.end = $scope.totalItems;              
                }

                if($scope.totalItems == 0)
                {
                    $scope.begin = -1;
                    $scope.end = 0;              
                }
            });                                  

            $scope.clasificaciones = ClasifVideosUTRM.find({
                  filter: {
                    where: $scope.condicion,
                    order: 'nombre ASC',
                    limit: $scope.numPerPage,
                    skip: $scope.currentPage - 1
                  }
            });

            $scope.mostrarbtnLimpiar = true;
        };

        $scope.limpiaBusqueda = function() {

            $scope.nombre_buscar = '';
            $scope.currentPage = 1;
            $scope.numPerPage = 5;
            $scope.begin = 0;
            $scope.end = 5;
            $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id};

            ClasifVideosUTRM
            .count({
              where: $scope.condicion
            })
            .$promise
            .then(function(resp) {
              $scope.totalItems = resp.count;
            });                                  

            $scope.clasificaciones = ClasifVideosUTRM.find({
                  filter: {
                    where: $scope.condicion,
                    order: 'nombre ASC',
                    limit: $scope.numPerPage,
                    skip: $scope.currentPage - 1
                  }
            });

            $scope.mostrarbtnLimpiar = false;
        };



        $scope.modificarClasificacion = function(DatosClasifActual) {

              $scope.idx_registro = $scope.clasificaciones.indexOf(DatosClasifActual);
      
              swal({
                title: 'Editar',   
                html: '<p><input id="input-field" class="form-control" value="'+DatosClasifActual.nombre+'">',
                showCancelButton: true,   
                closeOnConfirm: true
              }, 
              function() {   
                    ClasifVideosUTRM.prototype$updateAttributes(
                      { id: DatosClasifActual.id }, 
                      {
                        nombre: $('#input-field').val()
                    })
                    .$promise.then(function() {
                          $scope.clasificaciones[$scope.idx_registro].nombre = $('#input-field').val();
                    });
              });

        };

        $scope.altaClasificacion = function() {

              swal({
                title: 'Nueva clasificación',   
                html: '<p><input id="input-field" class="form-control">',
                showCancelButton: true,   
                closeOnConfirm: true
              }, 
              function() {   
                  ClasifVideosUTRM
                    .create({
                        nombre   :$('#input-field').val(),
                        campusId :$scope.currentUser.campus_pertenece_id
                    })
                    .$promise
                    .then(function(nuevo_grupo) {
          
                        $scope.totalItems++;
                        $scope.currentPage = 1;
                        $scope.begin = 0;
                        $scope.end = 5;

                        if($scope.end > $scope.totalItems)
                          $scope.end = $scope.totalItems;

                          $scope.clasificaciones = ClasifVideosUTRM.find({
                                filter: {
                                  where: $scope.condicion,
                                  order: 'nombre ASC',
                                  limit: $scope.numPerPage,
                                  skip: $scope.currentPage - 1
                                }
                          });
                    });                                  
              });

        };


      	$scope.eliminaClasificacion = function(DatosClasifActual) {

              $scope.idx_eliminar = $scope.clasificaciones.indexOf(DatosClasifActual);

              ClasifVideosUTRM.videos_contiene.count({ id: DatosClasifActual.id })
                .$promise
                .then(function(resultado) {
                  
                      if(resultado.count > 0)
                      {
                            
                              swal({
                                title: "Eliminar clasificación",
                                html: 'La clasificaci&oacute;n <strong>'+DatosClasifActual.nombre +'</strong> no se puede eliminar ya que tiene videos registrados</p>',
                                type: "warning",
                                showCConfirmButton: true,
                                showCancelButton: false,
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "Aceptar",
                                closeOnConfirm: true
                              });                            
                      }
                      else
                      {
                              swal({
                                title: "Eliminar clasificación",
                                html: '¿Desea eliminar la clasificaci&oacute;n <strong>'+DatosClasifActual.nombre +'</strong> ?',
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: "Eliminar",
                                cancelButtonText: "Cancelar",
                                closeOnConfirm: false,
                                closeOnCancel: true
                              }, function(){
                                    swal.disableButtons();

                                    ClasifVideosUTRM.deleteById({ id: DatosClasifActual.id })
                                    .$promise
                                    .then(function() {
                                            $scope.totalItems--;
                                            $scope.currentPage = 1;
                                            $scope.begin = 0;
                                            $scope.end = 5;
                                            $scope.mostrarbtnLimpiar = false;
                                            $scope.nombre_buscar = '';

                                            if($scope.end > $scope.totalItems)
                                              $scope.end = $scope.totalItems;

                                              $scope.clasificaciones = ClasifVideosUTRM.find({
                                                    filter: {
                                                      where: $scope.condicion,
                                                      order: 'nombre ASC',
                                                      limit: $scope.numPerPage,
                                                      skip: $scope.currentPage - 1
                                                    }
                                              });
                                            swal('ClasifVideosUTRM eliminada', '', 'success');
                                    });
                              });
                      }
              });

      	};

};


angular
    .module('inspinia')
    .controller('ListaClasifUTRMController' , ['$scope', '$q', 'ClasifVideosUTRM', '$state', '$stateParams', '$modal', ListaClasifUTRMController])
