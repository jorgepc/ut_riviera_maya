function ListaVideoController($scope, Galeria_video, SweetAlert, $sce, $state, $stateParams) {

        $scope.$parent.$parent.main.titulo_seccion = 'Videos';

        $scope.mostrarbtnLimpiar = false;

        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.begin = 0;
        $scope.end = 5;
        $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id };

        Galeria_video.count({
            where: $scope.condicion
        })
        .$promise
        .then(function(resp) {
            $scope.totalItems = resp.count;
            if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
            if($scope.totalItems == 0)
            {
                $scope.begin = -1;
                $scope.end = 0;
                $scope.inactivos = 0;
                $scope.activos = 0;
            }
        });


        $scope.lista_videos = Galeria_video.find({
              filter: {
                  where: $scope.condicion,
                  order: 'titulo ASC',
                  limit: $scope.numPerPage,
                  skip: $scope.currentPage - 1
              }
        });


        $scope.pageChanged = function () {

              $scope.begin = ($scope.currentPage - 1) * $scope.numPerPage;
              $scope.end = $scope.begin + $scope.numPerPage;
              if($scope.end > $scope.totalItems)
                $scope.end = $scope.totalItems;
        
              $scope.lista_videos = Galeria_video.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'titulo ASC',
                        limit: $scope.numPerPage,
                        skip:  $scope.begin
                    }
              });
        }

        $scope.muestraResultadosBusqueda = function() {

              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {and: [
                      {campusId: $scope.currentUser.campus_pertenece_id},
                      {titulo: {
                          like: $scope.nombre_buscar+ '.*', 
                          options: 'i'
                      }}
              ]};

              Galeria_video.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                    $scope.totalItems = resp.count;
                    if($scope.end > $scope.totalItems)
                        $scope.end = $scope.totalItems;
                    if($scope.totalItems == 0) {
                        $scope.begin = -1;
                        $scope.end = 0;              
                    }
              });                                  

              $scope.lista_videos = Galeria_video.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'titulo ASC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1
                    }
              });

              $scope.mostrarbtnLimpiar = true;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };


        $scope.limpiaBusqueda = function() {

              $scope.nombre_buscar = '';
              $scope.currentPage = 1;
              $scope.begin = 0;
              $scope.end = 5;
              $scope.condicion = {campusId: $scope.currentUser.campus_pertenece_id};

              Galeria_video.count({
                  where: $scope.condicion
              })
              .$promise
              .then(function(resp) {
                  $scope.totalItems = resp.count;
                  if($scope.end > $scope.totalItems)
                      $scope.end = $scope.totalItems;
                  if($scope.totalItems == 0) {
                      $scope.begin = -1;
                      $scope.end = 0;              
                  }
              });                                  

              $scope.lista_videos = Galeria_video.find({
                    filter: {
                        where: $scope.condicion,
                        order: 'titulo ASC',
                        limit: $scope.numPerPage,
                        skip: $scope.currentPage - 1
                    }
              });

              $scope.mostrarbtnLimpiar = false;
              $scope.client = 1;
              $scope.selectedRow = undefined;
        };

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        }

        $scope.muestraDatosRegistroSeleccionado = function(RegistroSeleccionado) {
              var index = $scope.lista_videos.indexOf(RegistroSeleccionado);
              $scope.DatosVideoActual = $scope.lista_videos[index];
              $scope.client = 2;
              $scope.selectedRow = index;
        };


        $scope.altaRegistro = function() {
              $state.go('galerias.alta_video');
        };


        $scope.eliminaVideo = function(RegistroSeleccionado) {

              $scope.idx_registro = $scope.lista_videos.indexOf(RegistroSeleccionado);

              swal({
                title: "Eliminar video",
                html: '¿Desea eliminar el video <strong>'+RegistroSeleccionado.titulo +'</strong> ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
              }, function(){
                    swal.disableButtons();

                    Galeria_video.deleteById({ id: RegistroSeleccionado.id })
                    .$promise
                    .then(function() {

                            $scope.client = 1;
                            $scope.selectedRow = undefined;

                            $scope.totalItems--;
                            $scope.currentPage = 1;
                            $scope.begin = 0;
                            $scope.end = 5;

                            if($scope.end > $scope.totalItems)
                                $scope.end = $scope.totalItems;
                            if($scope.totalItems == 0) {
                                $scope.begin = -1;
                                $scope.end = 0;              
                            }

                            $scope.lista_videos = Galeria_video.find({
                                  filter: {
                                      where: $scope.condicion,
                                      order: 'titulo ASC',
                                      limit: $scope.numPerPage,
                                      skip: $scope.currentPage - 1
                                  }
                            });

                            swal('Video eliminado', '', 'success');
                    });

              });

        };
};



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function AltaVideoController($scope, Galeria_video, $sce, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Nuevo video';
      $scope.txt_boton = 'Agregar';

      $scope.video = {
          titulo : "",
          url    : ""
      };

      $scope.video_preview = 'img/placeholder.png';

      $scope.verPreview = function() {
          $scope.video_preview = $scope.video.url;
      };

      $scope.trustSrc = function(src) {
          return $sce.trustAsResourceUrl(src);
      }

      $scope.guardaDatos = function() {

            Galeria_video
              .create({
                  titulo         :$scope.video.titulo,
                  url            :$scope.video.url,
                  fecha_creacion :Date(),
                  campusId       :$scope.currentUser.campus_pertenece_id
              })
              .$promise
              .then(function(nuevo_cliente) {
                    $state.go('galerias.lista_videos');
              });

      };

      $scope.cancelaForm = function() {
          $state.go('galerias.lista_videos');
      };

};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function EditaVideoController($scope, $q, Galeria_video, $stateParams, $sce, $state) {

      $scope.$parent.$parent.main.titulo_seccion = 'Editar video';
      $scope.txt_boton = 'Guardar';

      $q
        .all([
          Galeria_video.findById({ 
              id: $stateParams.id
          }).$promise
        ])
        .then(function(data) {
            $scope.video = data[0];
            $scope.video_preview = $scope.video.url;
      
        });

      $scope.verPreview = function() {
          $scope.video_preview = $scope.video.url;
      };

      $scope.trustSrc = function(src) {
          return $sce.trustAsResourceUrl(src);
      }

      $scope.guardaDatos = function() {
            
            Galeria_video.prototype$updateAttributes({ id: $scope.video.id }, $scope.video)
            .$promise
            .then(function(cliente) {
                  $state.go('galerias.lista_videos');
            })
            .catch(function(error) {
              if(error.status == 413)
                alert("El tamaño del archivo de imagen es muy grande");
            });

      };

      $scope.cancelaForm = function() {
          $state.go('galerias.lista_videos');
      };

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

angular
    .module('inspinia')
    .controller('ListaVideoController' , ['$scope', 'Galeria_video', 'SweetAlert', '$sce', '$state', '$stateParams', ListaVideoController])
    .controller('AltaVideoController'   , ['$scope', 'Galeria_video', '$sce', '$state', AltaVideoController])
    .controller('EditaVideoController'  , ['$scope', '$q', 'Galeria_video', '$stateParams', '$sce', '$state',  EditaVideoController])