function AuthService(User, $q, $rootScope, localStorageService) {

      function login(username, password) {

        return User
          .login({
              username: username,
              password: password})
          .$promise
          .then(function(response) {

                  return User.findById({ 
                      id: response.user.id,
                      filter: {
                            include: [
                              'campus_pertenece',
                              {
                                  relation: 'roles',
                                  scope: {
                                      fields:['id','name']
                                  }
                              }
                            ]
                      }
                  })
                  .$promise
                  .then(function(usuario) {
                        
                      var campus_pertenece_id =  usuario.campus_pertenece.id;
                      var campus_nombre = usuario.campus_pertenece.nombre;

                      $rootScope.currentUser = {
                          id: usuario.id,
                          tokenId: response.id,
                          username: usuario.username,
                          nombre: usuario.nombre,
                          perfil: usuario.roles[0].name,
                          campus_pertenece_id: campus_pertenece_id,
                          campus_nombre: campus_nombre,
                          estatus: 200
                      };
                      localStorageService.set('usuario', $rootScope.currentUser);
                 });

          })
          .catch(function(error) {
              if(error.status == 401){
                  $rootScope.currentUser = {
                      estatus: error.status
                  };
                }
          });
      }

      function logout() {
        return User
         .logout()
         .$promise
         .then(function() {
           $rootScope.currentUser = null;
           localStorageService.remove('usuario');
         });
      }

      function register(email, password) {
        return User
          .create({
           email: email,
           password: password
         })
         .$promise;
      }

      return {
        login: login,
        logout: logout,
        register: register
      };

};


angular
    .module('inspinia')
    .factory('AuthService', ['Usuario', '$q', '$rootScope', 'localStorageService', AuthService]);
