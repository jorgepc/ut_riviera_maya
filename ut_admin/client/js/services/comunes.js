function ListaCiudades() {

      var listaciudades = [
        {id :'', nombre: 'Todas'}
      ];
      return listaciudades
};

function ListaGrupoProductos() {

      var grupoProductos = [
        {valor :'Suplementos', nombre: 'Suplementos'},
        {valor :'Vestimenta', nombre: 'Vestimenta'},
        {valor :'Bebidas', nombre: 'Bebidas'}
      ];
      return grupoProductos
};


angular
    .module('inspinia')
    .factory('ListaCiudades', ListaCiudades)
    .factory('ListaGrupoProductos', ListaGrupoProductos);
