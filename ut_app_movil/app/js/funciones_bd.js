var db; //Variable de la conexion a la bd local

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// FUNCIONES PARA LA BD LOCAL ////////////////////////////////////////////////////////////////////////////

//Funcion que inicializa la base de datos
function inicia_bd(callback)
{
    if(enviroment == 'web')
    	db = window.openDatabase("ut_movil_bd", "1.0", "UT BD", 30 * 1024 * 1024);
    else if(enviroment == 'android')
    	db = window.sqlitePlugin.openDatabase({name: "ut_movil_bd.db", location: 1, androidDatabaseImplementation: 2, androidLockWorkaround: 1});
    else if(enviroment == 'ios')
    	db = window.sqlitePlugin.openDatabase({name: "ut_movil_bd.db", location: 1});

/*	reinicia_bd(function() {
		return;
	});
*/
	callback();
}


//Funcion que revisa si estan creadas las tablas o la  base de datos esta vacia
function checa_bd(callback)
{
    db.transaction(
        function(tx) {
            tx.executeSql("SELECT name FROM sqlite_master WHERE type='table'", this.txErrorHandler,
                function(tx, results) {
                    if (results.rows.length > 3)
                    {
						    db.transaction( function(tx) {
						            tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='version_app'", this.txErrorHandler, function(tx, results) {
						                    if (results.rows.length == 1)
						                    {
											    db.transaction( function(tx) {
											            tx.executeSql("SELECT * FROM version_app", this.txErrorHandler, function(tx, results) {
											                    if (results.rows.length == 0)
											                    {
												                    reinicia_bd(function() {
												                    	callback(0);
												                    });
											                    }
											                    else
											                    {
											                    	if(results.rows.item(0).version == VERSION)
												                    	callback(1);
												                    else
												                    {												                    	
													                    reinicia_bd(function() {
													                    	callback(0);
													                    });
												                    }
											                    }
											                });
											    });	
						                    }
						                    else
						                    {
							                    reinicia_bd(function() {
							                    	callback(0);
							                    });
						                    }
						            });
						    });	
                    }
                    else
	                    callback(0);
                });
        }
    );	

}


//Funcion que crea las tablas en la base de datos
function crea_tablas_datos(callback)
{
		//myApp.showPreloader('Creando bd local');
		var t1 = $.Deferred();
		var t2 = $.Deferred();
		var t3 = $.Deferred();
		var t4 = $.Deferred();
		var t5 = $.Deferred();
		var t6 = $.Deferred();
		var t7 = $.Deferred();
		var t8 = $.Deferred();
		var t9 = $.Deferred();
		var t10 = $.Deferred();
		var t11 = $.Deferred();
		var t12 = $.Deferred();
 
			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS eventos ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "nombre VARCHAR(150), " +
				          "ubicacion VARCHAR(150), " +
				          "descripcion VARCHAR(150), " +
				          "contenido TEXT, " +
				          "fechas_evento VARCHAR(95), " +
				          "img_principal TEXT, " +
				          "img_mapa_sitio TEXT, " +
				          "url_video TEXT)";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {
				          	_log('creada tabla eventos (1)');
				          	t1.resolve();
				      });
				  }
			 );

			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS programa ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "eventoid VARCHAR(35), " +
				          "orden INTEGER, " +
				          "fecha TEXT, " +
				          "hora VARCHAR(16), " +
				          "actividad VARCHAR(55), " +
				          "ponente VARCHAR(55))";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {

								db.transaction(function (tx) {
								        tx.executeSql('CREATE INDEX eventoid_idx ON programa (eventoid);', [], function (tx, results)
								          {
				          						_log('creada tabla programa (2)');
				          						t2.resolve();
								          }, function (tx) {
								          });
								});
				      });
				  }
			 );

			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS articulos ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "titulo VARCHAR(150), " +
				          "descripcion VARCHAR(150), " +
				          "contenido TEXT, " +
				          "fecha DATE, " +
				          "img_principal TEXT)";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {
				          	_log('creada tabla articulos (3)');
				          	t3.resolve();
				      });
				  }
			); 


			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS alumno ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "clave_alumno VARCHAR(18), "+
				          "nombres VARCHAR(50), "+
				          "apellidos VARCHAR(50), "+
				          "carrera VARCHAR(55), " +
				          "semestre VARCHAR(35), " +
				          "foto TEXT)";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {
				          	_log('creada tabla alumno (4)');
				          	t4.resolve();
				      });
				  }
			);

			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS carga_academica ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "materia VARCHAR(35), "+
				          "lunes VARCHAR(22), "+
				          "martes VARCHAR(22), "+
				          "miercoles VARCHAR(22), "+
				          "jueves VARCHAR(22), "+
				          "viernes VARCHAR(22), "+
				          "sabado VARCHAR(22))";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {
				          	_log('creada tabla carga_academica (5)');
				          	t5.resolve();
				      });
				  }
			);

			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS carreras ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "modalidad VARCHAR(60), " +
				          "nombre VARCHAR(90), "+
				          "icono TEXT, "+
				          "img_principal TEXT, "+
				          "contenido TEXT)";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {
				          	_log('creada tabla carreras (6)');
				          	t6.resolve();
				      });
				  }
			);

			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS galeria_fotos ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "titulo VARCHAR(90), " +
				          "fecha_creacion TEXT, "+
				          "data TEXT)";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {
				          	_log('creada tabla galeria_fotos (7)');
				          	t7.resolve();
				      });
				  }
			);

			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS galeria_videos ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "titulo VARCHAR(90), " +
				          "fecha_creacion TEXT, "+
				          "url TEXT)";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {
				          	_log('creada tabla galeria_videos (8)');
				          	t8.resolve();
				      });
				  }
			);

			db.transaction(
			  function(tx) {
			      var sql =
			          "CREATE TABLE IF NOT EXISTS tutorial ( " +
			          "mostrar_tutorial boolean DEFAULT 0)";

			      tx.executeSql(sql, this.txErrorHandler,
			          function(tx, results) {

						db.transaction(function (tx) {
						        tx.executeSql('INSERT INTO tutorial (mostrar_tutorial) VALUES (1)', [], function (tx, results)
						          {
				          			_log('creada tabla tutorial (9)');
				          			t9.resolve();
						          }, function (tx) {
						          });
						});
			      });
			  }
			);

			db.transaction(
			  function(tx) {
			      var sql =
			          "CREATE TABLE IF NOT EXISTS version_app ( " +
			          "version INTEGER)";

			      tx.executeSql(sql, this.txErrorHandler,
			          function(tx, results) {

						db.transaction(function (tx) {
						        tx.executeSql('INSERT INTO version_app (version) VALUES (?)', [VERSION], function (tx, results)
						          {
				          			_log('creada tabla version_app (10)');
				          			t10.resolve();
						          }, function (tx) {
						          });
						});
			      });
			  }
			);

			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS galeria_videos_utrm ( " +
				          "id VARCHAR(35) PRIMARY KEY, " +
				          "clasifid VARCHAR(35), " +
				          "titulo VARCHAR(90), " +
				          "fecha_creacion TEXT, "+
				          "url TEXT)";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {

								db.transaction(function (tx) {
								        tx.executeSql('CREATE INDEX clasifid_idx ON galeria_videos_utrm (clasifid);', [], function (tx, results)
								          {
				          						_log('creada tabla galeria_videos_utrm (11)');
				          						t11.resolve();
								          }, function (tx) {
								          });
								});
				      });
				  }
			);

			db.transaction(
				  function(tx) {
				      var sql =
				          "CREATE TABLE IF NOT EXISTS clasifvideos_utrm ( " +
					      "id VARCHAR(35) PRIMARY KEY, " +
				          "descripcion TEXT)";

				      tx.executeSql(sql, this.txErrorHandler,
				          function(tx, results) {
				          	_log('creada tabla clasifvideos_utrm (12)');
				          	t12.resolve();
				      });
				  }
			);



		$.when( t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12 ).done(function () {
		    _log('Todas las tablas creadas');
		    //myApp.hidePreloader();
		    callback();
		});

}


//Funcion que borra las tablas de la base de datos
function reinicia_bd (callback) {
		var t1 = $.Deferred();
		var t2 = $.Deferred();
		var t3 = $.Deferred();
		var t4 = $.Deferred();
		var t5 = $.Deferred();
		var t6 = $.Deferred();
		var t7 = $.Deferred();
		var t8 = $.Deferred();
		var t9 = $.Deferred();
		var t10 = $.Deferred();
		var t11 = $.Deferred();
		var t12 = $.Deferred();

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS eventos', [], function (tx, results)
		          {
          			_log('eliminada tabla eventos (1)');
          			t1.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS programa', [], function (tx, results)
		          {
          			_log('eliminada tabla programa (2)');
          			t2.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS articulos', [], function (tx, results)
		          {
          			_log('eliminada tabla articulos (3)');
          			t3.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS alumno', [], function (tx, results)
		          {
          			_log('eliminada tabla alumno (4)');
          			t4.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS carga_academica', [], function (tx, results)
		          {
          			_log('eliminada tabla carga_academica (5)');
          			t5.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS carreras', [], function (tx, results)
		          {
          			_log('eliminada tabla carreras (6)');
          			t6.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS galeria_fotos', [], function (tx, results)
		          {
          			_log('eliminada tabla galeria_fotos (7)');
          			t7.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS galeria_videos', [], function (tx, results)
		          {
          			_log('eliminada tabla galeria_fotos (8)');
          			t8.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS tutorial', [], function (tx, results)
		          {
          			_log('eliminada tabla tutorial (9)');
          			t9.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS version_app', [], function (tx, results)
		          {
          			_log('eliminada tabla version_app (10)');
          			t10.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS galeria_videos_utrm', [], function (tx, results)
		          {
          			_log('eliminada tabla galeria_videos_utrm (11)');
          			t11.resolve();
		          }, function (tx) {
		          });
		});

		db.transaction(function (tx) {
		        tx.executeSql('DROP TABLE IF EXISTS clasifvideos_utrm', [], function (tx, results)
		          {
          			_log('eliminada tabla clasifvideos_utrm (12)');
          			t12.resolve();
		          }, function (tx) {
		          });
		});

		$.when( t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12 ).done(function () {
		    _log('Todas las tablas eliminadas');
		    callback();
		});

}

