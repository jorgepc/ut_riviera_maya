// Initialize your app
var myApp = new Framework7({
    cache: true,
    modalButtonCancel: 'Cancelar',
    animateNavBackIcon: true,
    // Enable templates auto precompilation
    precompileTemplates: true,
    // Enabled pages rendering using Template7
	swipeBackPage: false,
	modalTitle: 'UT Riviera Maya',
	swipeBackPageThreshold: 1,
	swipePanel: "left",
	swipePanelCloseOpposite: true,
	pushState: false,
	pushStateRoot: undefined,
	pushStateNoAnimation: false,
	pushStateSeparator: '#!/',
    template7Pages: true
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: false
});


if(enviroment == 'web')
	window.onload = function(e){  onDeviceReady(); }
else if(enviroment == 'android' || enviroment == 'ios')
	document.addEventListener("deviceready", onDeviceReady, false);

var mySwiperEventos;

function onDeviceReady() {
    //jQuery.preloadCssImages();
    if($$(window).height() >= 1024)
    	$("#botones_principal").css("top",(  ( ($$(window).height()+50) / 2 ).toFixed() - 420) + "px");
    else if($$(window).height() >= 680)
    	$("#botones_principal").css("top",(  ( ($$(window).height()+50) / 2 ).toFixed() - 275) + "px");
    else
    	$("#botones_principal").css("top",(  ( ($$(window).height()+50) / 2 ).toFixed() - 225) + "px");

	document.addEventListener("backbutton", onBackKeyDown, false);

    inicia_bd(function (){
		checa_bd(function (existen_tablas) {
			if(existen_tablas == 0)
			{
					crea_tablas_datos(function() {
							myApp.showPreloader('Cargando datos de los eventos y artículos');

						    var d1 = $.Deferred();
						    var d2 = $.Deferred();

							//Este evento, al ser el primero debe checar si hay conexion, sino evitar que se ejecuten los demas
							descarga_datos_eventos(function (actualizado_eventos) {
								if(actualizado_eventos == -1)
								{
									d1.resolve();
									d2.resolve();
								}
								else
								{
									d1.resolve();
								}
							});

							descarga_datos_articulos(function (actualizado_articulos) {
								d2.resolve();
							});

							$.when( d1,d2 ).done(function ( v1,v2 ) {
									mySwiperEventos = myApp.swiper('.swiper-container', {
									    pagination:'.swiper-pagination',
									    autoplay: 3000,
									    loop: true,
									    autoplayDisableOnInteraction: false,
									    effect: 'slide' //fade, slide
									});

							    carga_perfil_lateral();
							    carga_lista_articulos(function(){
								    limita_articulos();
								    carga_lista_eventos(function(){
								    	myApp.hidePreloader();
								    });
							    });
			                	
							});

					});
			}
			else
			{
				    carga_perfil_lateral();
			}

			/*setTimeout(function(){ 
				
				    var d1 = $.Deferred();
				    var d2 = $.Deferred();
				    var d3 = $.Deferred();
				    var d4 = $.Deferred();
				    var d5 = $.Deferred();
				    var d6 = $.Deferred();
				    var d7 = $.Deferred();

				    var notif = myApp.addNotification({
				        closeIcon: false,
				        message: 'Buscando actualizaciones'
				    });	

					//Este evento, al ser el primero debe checar si hay conexion, sino evitar que se ejecuten los demas
					descarga_datos_eventos(function (actualizado_eventos) {
						if(actualizado_eventos == -1)
						{
							d1.resolve();
							d2.resolve();
							d3.resolve();
							d4.resolve();
							d5.resolve();
							d6.resolve();
							d7.resolve();
						}
						else
						{
						    carga_lista_eventos(function(){
						    });
							d1.resolve();
						}
					});

					descarga_datos_articulos(function (actualizado_articulos) {
					    carga_lista_articulos(function(){
						    limita_articulos();
					    });

						d2.resolve();
					});

					descarga_datos_fotos(function (actualizado_fotos) {
						d3.resolve();
					});

					descarga_datos_videos(function (actualizado_videos) {
						d4.resolve();
					});
					
					descarga_datos_carreras(function (actualizado_carreras) {
						d5.resolve();
					});

					descarga_lista_secciones_utrm(function (actualizado_secciones_utrm) {
						d6.resolve();
					});

					descarga_datos_videos_utrm(function (actualizado_videos_utrm) {
						d7.resolve();
					});

					$.when( d1,d2,d3,d4,d5,d6,d7 ).done(function ( v1,v2,v3,v4,v5,v6,v7 ) {
	                	myApp.closeNotification(notif);
					});

			}, 6000);*/
			
		});    	
    }); 
}


$$(document).on('pageInit', function (e) {

  		var page = e.detail.page;
  		_log('pagina actual:',page.name);

  		if(page.name == 'galeria') {
  			carga_fotos();
  			carga_videos();
  		}
  		else if(page.name == 'videos_utrm') {
  			carga_lista_secciones_utrm();
  		}
  		else if(page.name == 'oferta_educativa') {
  			carga_oferta_educativa();
  		}
  		else if(page.name == 'articulos') {
		    carga_lista_articulos(function(){
			    limita_articulos();
		    });
  		}
  		else if(page.name == 'eventos') {
		    carga_lista_eventos(function(){
		    });
  		}
  		else if(page.name == 'carga_academica') {
  			var fecha_date = new Date();

			myApp.showTab('#tab_'+fecha_date.getDay());
  			carga_materias_dia(fecha_date.getDay());
  		}
})

$$('.popup-login').on('opened', function () {
  $$("#clave_alumno").val('');
});


function regresa_pag_principal() {
	mainView.router.back();
	if(mainView.activePage.name == 'index'){

	    if($$(window).height() >= 1024)
	    	$("#botones_principal").css("top",(  ( ($$(window).height()+50) / 2 ).toFixed() - 420) + "px");
	    else if($$(window).height() >= 680)
	    	$("#botones_principal").css("top",(  ( ($$(window).height()+50) / 2 ).toFixed() - 275) + "px");
	    else
	    	$("#botones_principal").css("top",(  ( ($$(window).height()+50) / 2 ).toFixed() - 225) + "px");
	}

	return false;
}

function limita_articulos()
{
		$(".features_list_detailed li").hide();	
		size_li = $(".features_list_detailed li").size();
		x=4;
		$('.features_list_detailed li:lt('+x+')').show();
		$('#loadMore').click(function () {
			x= (x+1 <= size_li) ? x+1 : size_li;
			$('.features_list_detailed li:lt('+x+')').show();
			if(x == size_li){
				$('#loadMore').hide();
				$('#showLess').show();
			}
		});	
}
