//funcion para tomar foto (solo phonegap)
function tomar_foto () {
    navigator.camera.getPicture(onSuccessCamera, onFailCamera, { quality: 100,
        destinationType: Camera.DestinationType.DATA_URL,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 280,
        targetHeight: 280,
        correctOrientation: false,
        allowEdit: true
    });

}
function onSuccessCamera(imageData) {
    $$("#foto_alumno").attr("src","data:image/jpeg;base64," + imageData);

    db.transaction( function(tx) {
        tx.executeSql('UPDATE alumno SET foto = ?', ["data:image/jpeg;base64," + imageData], function (tx, results)
        {
        	actualiza_foto_central(function(){
        	})
        }, function (tx) {
          	_log('Error en la consulta');
        });
    });
}
function onFailCamera(message) {
    _log('Fallo de la camara: ' + message);
}



//Inicia sesion
function inicia_sesion()
{
	if($$("#clave_alumno").val() == '')
	{
	    myApp.alert('Por favor de escribir su número de alumno');
	    return;
	}
	myApp.showPreloader('Iniciando sesión');

	descarga_datos_alumno($$("#clave_alumno").val(), function(estatus) {        

	     myApp.hidePreloader();
	    if(estatus == 1)
	    {
	          carga_perfil_lateral();
	          myApp.closeModal('.popup-login');
	    }
	    else if(estatus == 0)
	    	myApp.alert('No se encontró la clave de alumno');
	    else if(estatus == -1)
	    	myApp.alert('No se tiene acceso al sistema central, probablemente no se cuente con una conexión a Internet','Error de conexión');
	});

}


//cierra sesion
function cierra_sesion()
{
	$("#lista_opciones_lateral").empty();
	$$("#txt_nombre_alumno").html('');
	$$("#txt_semestre").html('');
	$$("#txt_carrera").html('');
	$$("#btn_toma_foto").html('');
	$$("#foto_alumno").attr("src",'images/profile.jpg');
	$$("#lista_opciones_lateral").append('<li><a href="#" data-popup=".popup-login" class="open-popup"><img src="images/icons/white/lock.png" alt="" title="" /><span>Iniciar sesi&oacute;n</span></a></li>');

	db.transaction( function(tx) {
		  tx.executeSql("DELETE FROM carga_academica", this.txErrorHandler, function(tx, results) {
		  });
	});

	db.transaction( function(tx) {
		  tx.executeSql("DELETE FROM alumno", this.txErrorHandler, function(tx, results) {
		  });
	});
}


function carga_perfil_lateral()
{
	db.transaction(function (tx) {
	    tx.executeSql('SELECT * FROM alumno', [], function (tx, results)  
	    {
		      $("#lista_opciones_lateral").empty();
		      if (results.rows.length > 0)
		      {
					$$("#txt_nombre_alumno").html('<span>'+results.rows.item(0).nombres+'</span>'+results.rows.item(0).apellidos);
					$$("#txt_semestre").html(results.rows.item(0).semestre);
					$$("#txt_carrera").html(results.rows.item(0).carrera);
					$$("#foto_alumno").attr("src",(results.rows.item(0).foto == '' ? "images/profile.jpg" : results.rows.item(0).foto) );

					$$("#btn_toma_foto").html('<a href="#" onclick="tomar_foto();"><img src="images/icons/white/photos.png" alt="" title="" /></a> ');
					$$("#lista_opciones_lateral").append('<li><a href="carga_academica.html" class="close-panel"><img src="images/icons/white/briefcase.png" alt="" title="" /><span>Mi carga acad&eacute;mica</span></a></li>');
					$$("#lista_opciones_lateral").append('<li><a href="#" onclick="cierra_sesion();" class="close-panel"><img src="images/icons/white/lock.png" alt="" title="" /><span>Cerrar sesi&oacute;n</span></a></li>');
		      }
		      else
		      {
					$$("#txt_nombre_alumno").html('');
					$$("#txt_semestre").html('');
					$$("#txt_carrera").html('');
					$$("#btn_toma_foto").html('');
					$$("#foto_alumno").attr("src","images/profile.jpg");
					$$("#lista_opciones_lateral").append('<li><a href="#" data-popup=".popup-login" class="open-popup"><img src="images/icons/white/lock.png" alt="" title="" /><span>Iniciar sesi&oacute;n</span></a></li>');
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	    });
	});	
}



function carga_materias_dia(dia)
{
	var dias_semana = ['domingo','lunes','martes','miercoles','jueves','viernes','sabado'];
	var dias_semana_titulo = ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'];
	if(dia == 0)
	{
		$("#tab_"+dia).empty();
		$$("#tab_"+dia).append('<h1 style="font-size: 19px;margin-bottom: 15px;">No hay materias para este d&iacute;a</h1>');
	}
	db.transaction(function (tx) {
	    tx.executeSql('SELECT materia, '+dias_semana[dia]+' FROM carga_academica WHERE '+dias_semana[dia]+' <> \'\' ORDER BY '+dias_semana[dia]+' ASC', [], function (tx, results)  
	    {
		      $("#tab_"+dia).empty();
		      if (results.rows.length == 0)
		      {
		      		$$("#tab_"+dia).append('<h1 style="font-size: 19px;margin-bottom: 15px;">No hay materias para este d&iacute;a</h1>');
		      }
		      else
		      {

            		codigo = '<h4 style="font-size: 22px;margin: 5px 0 5px 0;">'+ dias_semana_titulo[dia] + '</h4><ul class="posts">';
            		var dia_mostrar = '';
            		var hora_proxima = (new Date().getHours() + 1 );

		            for (var i= 0; i < results.rows.length; i = i + 1)
		            {
			      		switch(dia) {
			      			case 1:
			      				dia_mostrar = results.rows.item(i).lunes;
			      				break;
			      			case 2:
			      				dia_mostrar = results.rows.item(i).martes;
			      				break;
			      			case 3:
			      				dia_mostrar = results.rows.item(i).miercoles;
			      				break;
			      			case 4:
			      				dia_mostrar = results.rows.item(i).jueves;
			      				break;
			      			case 5:
			      				dia_mostrar = results.rows.item(i).viernes;
			      				break;
			      			case 6:
			      				dia_mostrar = results.rows.item(i).sabado;
			      				break;
			      		};

                        var posicion = dia_mostrar.indexOf(':');
                        var hora_comparar = dia_mostrar.substring(0,posicion);
                        if(parseInt(hora_comparar) == hora_proxima && new Date().getDay() == dia)
                        	var flecha = '<img src="images/icons/black/back.png" width="22" alt="" title="" align="right"/>';
                        else
                        	var flecha = '';

                        codigo +=   '<li>'+
                                    '        <div class="post_date" style="width: 30%;">'+
                                    '            <span class="horas_materias">'+dia_mostrar+'</span>'+
                                    '        </div>'+
                                    '        <div class="post_title" style="width: 60%;">'+
                                    			flecha+'<h2>'+results.rows.item(i).materia+'</h2>'+
                                    '        </div>'+
                                    '</li>';

		            }
		            codigo += '</ul>';
		            $$("#tab_"+dia).html(codigo);
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	    });
	});		
}


function carga_detalle_evento(id)
{
	db.transaction(function (tx) {
	    tx.executeSql('SELECT * FROM eventos WHERE id = ?', [id], function (tx, results)  
	    {
		      if (results.rows.length == 0)
		      {
		      	myApp.alert("No hay detalle del evento");
		      }
		      else
		      {
		      		var codigo = '';
		      		var contenido_fin = '';
					var contenido_ini = '<div class="pages">'+
										'  <div data-page="detalle_evento" class="page no-toolbar no-navbar">'+
										'    <div class="page-content">'+
										'    '+
										'         <div id="barra_superior" class="navbarpages">'+
										'              <div class="nav_left_logo"><a href="#"><img src="images/colors/turquoise/logoheader.png" alt="" title="" /></a></div>'+
										'              <div class="nav_center_text">Evento</div>'+
										'              <div class="nav_right_button"><a href="#" onclick="regresa_pag_principal();"><img src="images/icons/white/back.png" alt="" title="" /></a></div>'+
										'         </div>'+

										'         <div id="pages_maincontent" style="background-image:url(\'images/colors/turquoise/fondo.jpg\');background-repeat:repeat;overflow: hidden;">'+
										//'         <div id="pages_maincontent" class="evento_bloque_sup" style="top:50px;background-image:url(\'images/colors/turquoise/fondo.jpg\');background-repeat:repeat;overflow-y: hidden;">'+
										'              <div class="post_single">'+

										'                    <div id="seccion_imagen" class="featured_image">'+
										'                        <img src="'+results.rows.item(0).img_principal+'" alt="" title="" />'+
										//'                        <div class="post_social">'+
										//'                        <a href="#" data-popup=".popup-social" class="open-popup"><img src="images/icons/white/heart.png" alt="" title="" /></a>'+
										//'                        </div> '+
										'                    </div>'+

										'                    <div id="descrip_evento" class="page_content" style="margin: 10px 0 10px 0;"> '+
										'                        <h1 style="font-size: 19px;margin-bottom: 15px;">'+results.rows.item(0).nombre+'</h1>'+
										'                        <span style="height: 25px; width:100%;"><img src="images/icons/black/map.png" width="17" height="17" alt="" title="" align="left"/> <h2 style="margin-left:30px;">'+results.rows.item(0).ubicacion+'</h2></span>'+
										'                        <span style="height: 25px; width:100%;"><img src="images/icons/black/blog.png" width="17" height="17" alt="" title="" align="left"/> <h2 style="margin-left:30px;">'+results.rows.item(0).fechas_evento+'</h2></span>'+
										'                    </div>'+

										'                  <div id="barra_tabs" class="buttons-row" style="margin: 0 10px;">'+
										'                        <a href="#tab1" class="tab-link active button">Descripci&oacute;n</a>'+
										'                        <a href="#tab2" class="tab-link button">Programa</a>'+
										'                        <a href="#tab3" class="tab-link button">Mapa del sitio</a>'+
										'                  </div>'+
										//'              </div>'+


										//'              <div class="post_single">'+
										'                  <div id="contenido_tabs" class="tabs-animated-wrap" style="margin: 10px 10px 0 10px; width: 95%;">'+
										'                        <div class="tabs">'+
										'                              <div id="tab1" class="tab active" style="height:50px;overflow-y:scroll;">'+
																			results.rows.item(0).contenido+
										'								</div>'+

										'                              <div id="tab2" class="tab" style="height:50px;overflow-y:scroll;">';
										
					    contenido_fin = '                              </div> '+

										'                              <div id="tab3" class="tab" style="height:50px;overflow-y:scroll;">'+
										'                              		<div style="background-image:url(\''+results.rows.item(0).img_mapa_sitio+'\'); background-size: contain; background-repeat: no-repeat;width: 100%; height: 300px;"></div>'+
										'                              </div>'+
										'                        </div>'+
										'                  </div>'+
										'              </div>'+

										'         </div>'+

										'    </div>'+
										'  </div>'+
										'</div>';

					db.transaction(function (tx) {
					    tx.executeSql('SELECT * FROM programa WHERE eventoid = ? ORDER BY orden ASC', [id], function (tx, results)  
					    {
						      //$("#tab2").empty();
						      if (results.rows.length == 0)
						      {
						      		$$("#tab2").html('<h1 style="font-size: 19px;margin-bottom: 15px;">EL evento no cuenta con ning&uacute;n programa</h1>');
						      }
						      else
						      {
						            var fecha = results.rows.item(0).fecha;
				            		var fecha_date = new Date(results.rows.item(0).fecha);

				            		codigo = '<h1 style="font-size: 22px;margin: 5px 0 5px 0;">'+fecha_date.getDate() + ' de ' + lista_meses[fecha_date.getMonth()] + ' del ' + fecha_date.getFullYear() + '</h1><ul class="posts">';

						            for (var i= 0; i < results.rows.length; i = i + 1)
						            {
						            	if(fecha != results.rows.item(i).fecha)
						            	{
						            		fecha = results.rows.item(i).fecha;
						            		fecha_date = new Date(results.rows.item(i).fecha);

						            		codigo += '</ul> <h1 style="font-size: 22px;margin: 5px 0 5px 0;">'+fecha_date.getDate() + ' de ' + lista_meses[fecha_date.getMonth()] + ' del ' + fecha_date.getFullYear() +'</h1><ul class="posts">';
						            	}

		                                codigo +=   '<li>'+
			                                        '    <div class="post_entry">'+
			                                        '        <div class="post_date">'+
			                                        '            <span class="horas">'+results.rows.item(i).hora+'</span>'+
			                                        '        </div>'+
			                                        '        <div class="post_title">'+
			                                        '        <h2>'+results.rows.item(i).actividad+'</h2>'+
			                                        '        <h4>'+results.rows.item(i).ponente+'</h4>'+
			                                        '        </div>'+
			                                        '    </div>'+
			                                        '</li>';

						            }
						            codigo += '</ul>';
						            //$$("#tab2").append(codigo);
						            mainView.router.loadContent(contenido_ini + codigo + contenido_fin);

									setTimeout(function() {
										var alto_panel_superior = $("#barra_superior").height() + $("#seccion_imagen").height() + $("#descrip_evento").height() + $("#barra_tabs").height();
										var alto_panel_inferior = $$(window).height() - alto_panel_superior - 24;

										$("#contenido_tabs").css("height",alto_panel_inferior + "px");
										
										$("#tab1").css("height",alto_panel_inferior + "px");
										$("#tab2").css("height",alto_panel_inferior + "px");
										$("#tab3").css("height",(alto_panel_inferior) + "px");
									},500);

						            //mainView.router.loadPage('detalle_evento.html');
						      }
					    }, function (tx) {
					        _log('Error en la consulta');
					    });
					});	
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	    });
	});	
}


function carga_lista_eventos(callback)
{
	db.transaction(function (tx) {
	    tx.executeSql('SELECT * FROM eventos ORDER BY nombre ASC', [], function (tx, results)  
	    {		      
		      
	          mySwiperEventos.removeAllSlides();
		      if (results.rows.length == 0)
		      {
                    mySwiperEventos.appendSlide('<div class="swiper-slide"><h3>No hay ning&uacute;n evento</h3></div>');
                    mySwiperEventos.update(true);
                    mySwiperEventos.stopAutoplay();
                    mySwiperEventos.lockSwipes();
					callback();
		      }
		      else
		      {
		      		//mySwiperEventos.stopAutoplay();
		            
		            for (var i= 0; i < results.rows.length; i = i + 1)
		            	mySwiperEventos.appendSlide('<div class="swiper-slide" style="background-image:url('+results.rows.item(i).img_principal+'); background-size: contain; background-repeat: no-repeat;" onclick="carga_detalle_evento(\''+results.rows.item(i).id+'\')"></div>');
						//$$("#lista_eventos").append('<div class="swiper-slide" style="background-image:url('+results.rows.item(i).img_principal+'); background-size: contain; background-repeat: no-repeat;" onclick="carga_detalle_evento(\''+results.rows.item(i).id+'\')"></div>');

					mySwiperEventos.update(true);
					if(results.rows.length > 1)
					{
					    mySwiperEventos.startAutoplay();
					    mySwiperEventos.unlockSwipes();
					}
					else
					{
					    mySwiperEventos.stopAutoplay();
					    mySwiperEventos.lockSwipes();
					}
		            
		            callback();
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	        callback();
	    });
	});	
}


function carga_detalle_articulo(id)
{
	//mainView.router.loadPage('detalle_articulo.html');
	db.transaction(function (tx) {
	    tx.executeSql('SELECT * FROM articulos WHERE id = ?', [id], function (tx, results)  
	    {
		      if (results.rows.length == 0)
		      {
		      		myApp.alert("No hay detalle del articulo");
		      }
		      else
		      {
					var fecha = new Date(results.rows.item(0).fecha);
					mainView.router.loadContent('<div class="pages">'+
												'  <div data-page="detalle_articulo" class="page no-toolbar no-navbar">'+
												'    <div class="page-content">'+

												'         <div id="barra_superior" class="navbarpages">'+
												'              <div class="nav_left_logo"><a href="#"><img src="images/colors/turquoise/logoheader.png" alt="" title="" /></a></div>'+
												'              <div class="nav_center_text">Art&iacute;culo</div>'+
												'              <div class="nav_right_button"><a href="#" onclick="regresa_pag_principal();"><img src="images/icons/white/back.png" alt="" title="" /></a></div>'+
												'         </div>'+

												'         <div id="pages_maincontent" style="background-image:url(\'images/colors/turquoise/fondo.jpg\');background-repeat:repeat;overflow: hidden;">'+

												'              <div class="post_single">'+

												'                    <div id="seccion_imagen" class="featured_image">'+
												'                        <img src="'+(results.rows.item(0).img_principal == '' ? 'images/articulo_default.jpg' : results.rows.item(0).img_principal)+'" alt="" title="" />'+
												'                        <div class="post_title_single"><h2>'+results.rows.item(0).titulo+'</h2></div>'+
												//'                        <div class="post_social">'+
												//'                        	<a href="#" data-popup=".popup-social" class="open-popup"><img src="images/icons/white/heart.png" alt="" title="" /></a>              '+
												//'                        </div> '+
												'                    </div>'+

												'                    <div id="fecha_articulo" class="page_content" style="margin: 10px 0 5px 0;"> '+
												'                        <span style="height: 25px; width:100%;"><img src="images/icons/black/blog.png" width="17" height="17" alt="" title="" align="left"/> <h2 style="margin-left:30px;">'+fecha.getDate()+'/'+lista_meses[fecha.getMonth()]+'/'+fecha.getFullYear()+'</h2></span>'+
												'                    </div>'+

												'                    <div id="articulo_contenido" style="height:239px;overflow-y:scroll;">'+
														    				results.rows.item(0).contenido+
												'                    </div>'+
												'              </div>'+

												'         </div>'+
												'    </div>'+
												'  </div>'+
												'</div>');

												setTimeout(function() {
													var alto_panel_superior = $("#barra_superior").height() + $("#seccion_imagen").height() + $("#fecha_articulo").height();
													var alto_panel_inferior = $$(window).height() - alto_panel_superior - 18;
													$("#articulo_contenido").css("height",alto_panel_inferior + "px");
												},500);
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	    });
	});	
}


function carga_lista_articulos(callback)
{
	
	$("#seccion_imagen_eventos").css("height",($$(window).width() * 0.475).toFixed() + "px");
	$("#pag_seccion_imagen_eventos").css("top",($$(window).width() * 0.416).toFixed() + "px");

	setTimeout(function() {
		var alto_panel_superior = $("#seccion_imagen_eventos").height() + $("#txt_ultimos_articulos").height();
		var alto_panel_inferior = $$(window).height() - alto_panel_superior - $(".toolbar").height() - 35;
		$("#seccion_lista_articulos").css("height",alto_panel_inferior + "px");
	},500);

	db.transaction(function (tx) {
	    tx.executeSql('SELECT * FROM articulos ORDER BY fecha DESC', [], function (tx, results)  
	    {
		      $("#lista_articulos").empty();
		      if (results.rows.length == 0)
		      {
					myApp.showPreloader('Descargando artículos');
					descarga_datos_articulos(function (actualizado_articulos) {

							db.transaction(function (tx) {
							    tx.executeSql('SELECT * FROM articulos ORDER BY fecha DESC', [], function (tx, results)  
							    {
								      if (results.rows.length == 0)
								      {
						                    $$("#lista_articulos").append('<li><h3>No hay ning&uacute;n art&iacute;culo</h3></li>');
								      }
								      else
								      {
								            for (var i= 0; i < results.rows.length; i = i + 1)
								            {
												$$("#lista_articulos").append('<li>'+
																		      '    <a href="#" style="color:#000;" onclick="carga_detalle_articulo(\''+results.rows.item(i).id+'\')">'+
																		      '    <div class="feat_small_icon"><img src="'+(results.rows.item(i).img_principal == '' ? 'images/icons/botones/ut.png' : results.rows.item(i).img_principal)+'" alt="" title="" /></div>'+
																		      '    <div class="feat_small_details">'+
																			  '        <h4>'+results.rows.item(i).titulo+'</h4>'+
																			  '        '+results.rows.item(i).descripcion+''+
																		      '    </div>'+
																		      '    <div class="view_more"><img src="images/delante.png" alt="" title="" /></div>'+
																		      '    </a>'+
																	          '</li>');
								            }
								            $$("#lista_articulos").append('<div class="clear"></div><div id="loadMore"><img src="images/load_posts.png" alt="" title="" /></div><div id="showLess"><img src="images/load_posts_disabled.png" alt="" title="" /></div>');
								      }
								      myApp.hidePreloader();
								      callback();
							    }, function (tx) {
							        _log('Error en la consulta');
							    });
							});
					});
		      }
		      else
		      {
		            for (var i= 0; i < results.rows.length; i = i + 1)
		            {
						$$("#lista_articulos").append('<li>'+
												      '    <a href="#" style="color:#000;" onclick="carga_detalle_articulo(\''+results.rows.item(i).id+'\')">'+
												      '    <div class="feat_small_icon"><img src="'+(results.rows.item(i).img_principal == '' ? 'images/icons/botones/ut.png' : results.rows.item(i).img_principal)+'" alt="" title="" /></div>'+
												      '    <div class="feat_small_details">'+
													  '        <h4>'+results.rows.item(i).titulo+'</h4>'+
													  '        '+results.rows.item(i).descripcion+''+
												      '    </div>'+
												      '    <div class="view_more"><img src="images/delante.png" alt="" title="" /></div>'+
												      '    </a>'+
											          '</li>');
		            }
		            $$("#lista_articulos").append('<div class="clear"></div><div id="loadMore"><img src="images/load_posts.png" alt="" title="" /></div><div id="showLess"><img src="images/load_posts_disabled.png" alt="" title="" /></div>');
		            callback();
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	        callback();
	    });
	});	
}


function carga_fotos()
{
	db.transaction(function (tx) {
	    tx.executeSql('SELECT * FROM galeria_fotos ORDER BY titulo', [], function (tx, results)  
	    {
		      $("#photoslist").empty();
		      if (results.rows.length == 0)
		      {
					myApp.showPreloader('Descargando fotos');
					descarga_datos_fotos(function (actualizado_fotos) {

							db.transaction(function (tx) {
							    tx.executeSql('SELECT * FROM galeria_fotos ORDER BY titulo', [], function (tx, results)  
							    {
								      if (results.rows.length == 0)
								      {
								      		$$("#photoslist").append('<h3>No hay fotos para mostrar</h3>');
								      }
								      else
								      {
								            for (var i= 0; i < results.rows.length; i = i + 1)
								            	$$("#photoslist").append('<li id="foto_'+results.rows.item(i).id+'"><span class="preloader"></span></li>');

								            setTimeout(function() {
									            for (var i= 0; i < results.rows.length; i = i + 1)
									            	$$("#foto_"+results.rows.item(i).id).html('<a rel="gallery-3" href="'+results.rows.item(i).data+'" title="'+results.rows.item(i).titulo+'" class="swipebox"><img src="'+results.rows.item(i).data+'" alt="image"/></a>');

									            $$("#photoslist").append('<div class="clearleft"></div>');
									            $(".swipebox").swipebox();
								        	}, 1500);
								      }
								      myApp.hidePreloader();
							    }, function (tx) {
							        _log('Error en la consulta');
							    });
							});	
					});
		      }
		      else
		      {
		            for (var i= 0; i < results.rows.length; i = i + 1)
		            	$$("#photoslist").append('<li id="foto_'+results.rows.item(i).id+'"><span class="preloader"></span></li>');

		            setTimeout(function() {
			            for (var i= 0; i < results.rows.length; i = i + 1)
			            	$$("#foto_"+results.rows.item(i).id).html('<a rel="gallery-3" href="'+results.rows.item(i).data+'" title="'+results.rows.item(i).titulo+'" class="swipebox"><img src="'+results.rows.item(i).data+'" alt="image"/></a>');

		            	//$$("#photoslist").append('<li><a rel="gallery-3" href="'+results.rows.item(i).data+'" title="'+results.rows.item(i).titulo+'" class="swipebox"><img src="'+results.rows.item(i).data+'" alt="image"/></a></li>');
			            $$("#photoslist").append('<div class="clearleft"></div>');
			            $(".swipebox").swipebox();
		        	}, 1500);
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	    });
	});	


	$("a.switcher").bind("click", function(e){
		e.preventDefault();
		var theid = $(this).attr("id");
		var theproducts = $("ul#photoslist");
		var classNames = $(this).attr('class').split(' ');
		
		
		if($(this).hasClass("active")) {
			// if currently clicked button has the active class
			// then we do nothing!
			return false;
		} else {
			// otherwise we are clicking on the inactive button
			// and in the process of switching views!

  			if(theid == "view13") {
				$(this).addClass("active");
				$("#view11").removeClass("active");
				$("#view11").children("img").attr("src","images/switch_11.png");
				
				$("#view12").removeClass("active");
				$("#view12").children("img").attr("src","images/switch_12.png");
			
				var theimg = $(this).children("img");
				theimg.attr("src","images/switch_13_active.png");
			
				// remove the list class and change to grid
				theproducts.removeClass("photo_gallery_11");
				theproducts.removeClass("photo_gallery_12");
				theproducts.addClass("photo_gallery_13");

			}
			
			else if(theid == "view12") {
				$(this).addClass("active");
				$("#view11").removeClass("active");
				$("#view11").children("img").attr("src","images/switch_11.png");
				
				$("#view13").removeClass("active");
				$("#view13").children("img").attr("src","images/switch_13.png");
			
				var theimg = $(this).children("img");
				theimg.attr("src","images/switch_12_active.png");
			
				// remove the list class and change to grid
				theproducts.removeClass("photo_gallery_11");
				theproducts.removeClass("photo_gallery_13");
				theproducts.addClass("photo_gallery_12");

			} 
			else if(theid == "view11") {
				$("#view12").removeClass("active");
				$("#view12").children("img").attr("src","images/switch_12.png");
				
				$("#view13").removeClass("active");
				$("#view13").children("img").attr("src","images/switch_13.png");
			
				var theimg = $(this).children("img");
				theimg.attr("src","images/switch_11_active.png");
			
				// remove the list class and change to grid
				theproducts.removeClass("photo_gallery_12");
				theproducts.removeClass("photo_gallery_13");
				theproducts.addClass("photo_gallery_11");

			} 
			
		}

	});	

}


function carga_videos()
{
	db.transaction(function (tx) {
	    tx.executeSql('SELECT * FROM galeria_videos ORDER BY titulo', [], function (tx, results)  
	    {
		      $("#listaVideos").empty();
		      if (results.rows.length == 0)
		      {
					myApp.showPreloader('Descargando videos');
					descarga_datos_videos(function (actualizado_videos) {

							db.transaction(function (tx) {
							    tx.executeSql('SELECT * FROM galeria_videos ORDER BY titulo', [], function (tx, results)  
							    {
								      if (results.rows.length == 0)
								      {
						                    $$("#listaVideos").append('<h3>No hay ning&uacute;n video</h3>');
								      }
								      else
								      {
								            for (var i= 0; i < results.rows.length; i = i + 1)
								            {
							                    $$("#listaVideos").append('<h3>'+results.rows.item(i).titulo+'</h3>'+
														                  '<div id="frameVideo_'+results.rows.item(i).id+'" class="videocontainer">'+
														                  '<span class="preloader"></span>'+
														                  '</div>');
								            }

								            setTimeout(function() {
									            for (var i= 0; i < results.rows.length; i = i + 1)
									            {
								                    $$("#frameVideo_"+results.rows.item(i).id).html('<iframe width="100%" height="180" src="'+results.rows.item(i).url+'" frameborder="0"></iframe>');
									            }
								        	}, 1500);
								      }
								      myApp.hidePreloader();
							    }, function (tx) {
							        _log('Error en la consulta');
							    });
							});
					});
		      }
		      else
		      {
		            for (var i= 0; i < results.rows.length; i = i + 1)
		            {
	                    $$("#listaVideos").append('<h3>'+results.rows.item(i).titulo+'</h3>'+
								                  '<div id="frameVideo_'+results.rows.item(i).id+'" class="videocontainer">'+
								                  '<span class="preloader"></span>'+
								                  '</div>');
		            }

		            setTimeout(function() {
			            for (var i= 0; i < results.rows.length; i = i + 1)
			            {
		                    $$("#frameVideo_"+results.rows.item(i).id).html('<iframe width="100%" height="180" src="'+results.rows.item(i).url+'" frameborder="0"></iframe>');
			            }
		        	}, 1500);
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	    });
	});	
}



function carga_detalle_carrera(id)
{
	db.transaction(function (tx) {
	    tx.executeSql('SELECT * FROM carreras WHERE id = ?', [id], function (tx, results)  
	    {
			myApp.popup('<div class="popup popup-about">'+
										'  <div data-page="detalle_carrera" class="page no-toolbar no-navbar">'+
										'    <div class="page-content">'+

										'         <div id="barra_superior_carrera" class="navbarpages">'+
										'              <div class="nav_left_logo"><a href="#"><img src="images/colors/turquoise/logoheader.png" alt="" title="" /></a></div>'+
										'              <div class="nav_center_text">Detalle de la carrera</div>'+
										'              <div class="nav_right_button"><a href="#" class="close-popup"><img src="images/icons/white/menu_close.png" alt="" title="" /></a></div>'+
										'         </div>'+

										'         <div id="pages_maincontent" style="background-image:url(\'images/colors/turquoise/fondo.jpg\');background-repeat:repeat;overflow: hidden;">'+

										'              <div class="post_single">'+

										'                    <div id="seccion_imagen_carrera" class="featured_image">'+
										'                        <img src="'+(results.rows.item(0).img_principal == '' ? 'images/articulo_default.jpg' : results.rows.item(0).img_principal)+'" alt="" title="" style="width:100%"/>'+
										'                    </div>'+

										'                    <div id="seccion_modalidad" class="page_content" style="margin: 10px 0 5px 0;"> '+
										'                        <h1 style="font-size: 19px;margin-bottom: 15px;color:#000;">'+results.rows.item(0).nombre+'</h1>'+
										'                        <span style="height: 25px; width:100%;"><img src="images/icons/black/user.png" width="17" height="17" alt="" title="" align="left"/> <h2 style="margin-left:30px;color:#000;">'+results.rows.item(0).modalidad+'</h2></span>'+
										'                    </div>'+

										'                    <div id="carrera_contenido" style="height:239px;overflow-y:scroll;">'+
												    				results.rows.item(0).contenido+
										'                    </div>'+
										'              </div>'+

										'         </div>'+
										'    </div>'+
										'  </div>'+
										'</div>');

										setTimeout(function() {
											var alto_panel_superior = $("#barra_superior_carrera").height() + $("#seccion_imagen_carrera").height() + $("#seccion_modalidad").height();
											var alto_panel_inferior = $$(window).height() - alto_panel_superior - 18;
											$("#carrera_contenido").css("height",alto_panel_inferior + "px");
										},500);

	    }, function (tx) {
	        _log('Error en la consulta');
	    });
	});	
}



function carga_oferta_educativa()
{
	db.transaction(function (tx) {
	    tx.executeSql('SELECT id, nombre, icono FROM carreras ORDER BY modalidad, nombre', [], function (tx, results)  
	    {
		      $("#listaCarreras").empty();
		      if (results.rows.length == 0)
		      {
					myApp.showPreloader('Descargando oferta educativa');
					descarga_datos_carreras(function (actualizado_carreras) {

							db.transaction(function (tx) {
							    tx.executeSql('SELECT id, nombre, icono FROM carreras ORDER BY modalidad, nombre', [], function (tx, results)  
							    {
								      if (results.rows.length == 0)
								      {
						                    $$("#listaCarreras").append('<h3>No hay ninguna lista de carreras</h3>');
								      }
								      else
								      {
								            for (var i= 0; i < results.rows.length; i = i + 1)
							                    $$("#listaCarreras").append('<li><a href="#" onclick="carga_detalle_carrera(\''+results.rows.item(i).id+'\')"><img src="'+results.rows.item(i).icono+'" alt="" title="" /><span>'+results.rows.item(i).nombre+'</span></a></li>');
								      }
								      myApp.hidePreloader();
							    }, function (tx) {
							        _log('Error en la consulta');
							    });
							});
					});
		      }
		      else
		      {
		            for (var i= 0; i < results.rows.length; i = i + 1)
	                    $$("#listaCarreras").append('<li><a href="#" onclick="carga_detalle_carrera(\''+results.rows.item(i).id+'\')"><img src="'+results.rows.item(i).icono+'" alt="" title="" /><span>'+results.rows.item(i).nombre+'</span></a></li>');
		      }
	    }, function (tx) {
	        _log('Error en la consulta');
	    });
	});	
}



function onBackKeyDown() {
   myApp.confirm('&iquest;Desea salir de la aplicaci&oacute;n?', function () {
      navigator.app.exitApp();
   });
}
