//Funcion que envia la foto del alumno al sistema central
function actualiza_foto_central(callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
			db.transaction(function (tx) {
			    tx.executeSql('SELECT * FROM alumno', [], function (tx, results)  
			    {
				      if (results.rows.length > 0)
				      {
			                //Primero checamos si existe la foto del alumno
							$.ajax({
							    method: 'GET', 
							    url: url + '/FotosAlumnos?filter={"where":{"clave_alumno":"'+results.rows.item(0).clave_alumno+'"},"fields":["id"]}&access_token='+logueado.token,
							    dataType: 'json',
							    success: function(data)
							    {
							    	if(data.length > 0)
							    	{
											//Actualiza la foto en central
											$.ajax({
										        method: 'PUT',
										        url: url + "/FotosAlumnos/"+data[0].id+'?access_token='+logueado.token,
										        data: {
										        	foto: results.rows.item(0).foto
										        },
										        dataType: 'json',
										        success: function(data)
										        {
						                                logout_sistema(logueado.token, function(estatus) {
						                                });
								                		callback(1);
										        },
										        error: function(xhr) {
							                            logout_sistema(logueado.token, function(estatus) {
							                            });
								                		callback(1);
										        }
										    });
							    	}
							    	else
							    	{
											//inserta la foto en central
											$.ajax({
										        method: 'POST',
										        url: url + "/FotosAlumnos?access_token="+logueado.token,
										        data: {
													alumnoId     : results.rows.item(0).id,
													clave_alumno : results.rows.item(0).clave_alumno,
													foto         : results.rows.item(0).foto
										        },
										        dataType: 'json',
										        success: function(data)
										        {
						                                logout_sistema(logueado.token, function(estatus) {
						                                });
								                		callback(1);
										        },
										        error: function(xhr) {
							                            logout_sistema(logueado.token, function(estatus) {
							                            });
								                		callback(1);
										        }
										    });
							    	}
							    },
							    error: function(xhr) {
							        logout_sistema(logueado.token, function(estatus) {
							        });
							    	callback(0);
							    }
							});			              
				      }
				      else
				      {
	                        logout_sistema(logueado.token, function(estatus) {
	                        });
							callback(0);
				      }
			    }, function (tx) {
			        _log('Error en la consulta');
			    });
			});
		}
        else if(logueado.estatus == -1)
        {
				callback(0);
        }
	});
}


//Funcion que busca al alumno por medio de su clave y descarga sus datos y su carga academica
function descarga_datos_alumno(clave_alumno, callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
				$.ajax({
			        method: 'GET', 
			        url: url + '/Alumnos?filter={"where":{"and":[{"campusId":"'+campusid+'"},{"clave_alumno":"'+clave_alumno+'"}]},"include":["cargaAcademica"]}&access_token='+logueado.token,
			        dataType: 'json',
			        success: function(data)
			        {
			        	if(data.length > 0)
			        	{
							db.transaction( function(tx) {
								  tx.executeSql("DELETE FROM carga_academica", this.txErrorHandler, function(tx, results) {
								  });
							});

							db.transaction( function(tx) {
							    tx.executeSql("DELETE FROM alumno", this.txErrorHandler, function(tx, results) {

									    db.transaction(function (tx) {
									            tx.executeSql('INSERT INTO alumno (id, clave_alumno, nombres, apellidos, carrera, semestre, foto) VALUES (?,?,?,?,?,?,?)',
									              [data[0].id, data[0].clave_alumno, data[0].nombres, data[0].apellidos, data[0].carrera, data[0].semestre, ''], function (tx, results)
									              {
									                	var alumnoId = data[0].id;
									                	var d1 = $.Deferred();
									                	if(data[0].cargaAcademica.length == 0)
									                	{
									                		_log('materias cargadas');
									                		d1.resolve();
									                	}
									                	else
									                	{
											                	$$.each(data[0].cargaAcademica, function(index, fila)
											                	{
																	    db.transaction(function (tx) {
																	            tx.executeSql('INSERT INTO carga_academica (id, materia, lunes, martes, miercoles, jueves, viernes, sabado) VALUES (?,?,?,?,?,?,?,?)',
																	              [fila.id, fila.materia, fila.lunes, fila.martes, fila.miercoles, fila.jueves, fila.viernes, fila.sabado], function (tx, results)
																	              {
																	              	if( (index+1) == data[0].cargaAcademica.length ){
																                		_log('materias cargadas');
																                		d1.resolve();
																	              	}

																	              }, function (tx) {
																	                    _log('Error en la consulta');
																	              });
																	    });
											                	});									                		
									                	}

														$.when( d1 ).done(function ( v1 ) {

																//Obtenemos la foto del alumno
																$.ajax({
															        method: 'GET', 
															        url: url + '/FotosAlumnos?filter={"where":{"clave_alumno":"'+clave_alumno+'"},"fields":["id"]}&access_token='+logueado.token,
															        dataType: 'json',
															        success: function(data)
															        {
															        	if(data.length > 0)
															        	{
																				//Actualiza el id del alumno en la bd central (por si no esta correcto)
																				$.ajax({
																			        method: 'PUT',
																			        url: url + "/FotosAlumnos/"+data[0].id+'?access_token='+logueado.token,
																			        data: {
																			        	alumnoId: alumnoId
																			        },
																			        dataType: 'json',
																			        success: function(data)
																			        {
																						    db.transaction(function (tx) {
																						            tx.executeSql('UPDATE alumno SET foto = ? WHERE clave_alumno = ?', [data.foto, clave_alumno], function (tx, results)
																						              {
																			                                logout_sistema(logueado.token, function(estatus) {
																			                                });
																					                		callback(1);

																						              }, function (tx) {
																						                    _log('Error en la consulta');
																						              });
																						    });
																			        },
																			        error: function(xhr) {
															                                logout_sistema(logueado.token, function(estatus) {
															                                });
																	                		callback(1);
																			        }
																			    });
															        	}
															        	else
															        	{
												                                logout_sistema(logueado.token, function(estatus) {
												                                });
														                		callback(1);
															        	}
															        },
															        error: function(xhr) {
										                                logout_sistema(logueado.token, function(estatus) {
										                                });
															        	callback(0);
															        }
															    });
														});

									              }, function (tx) {
									                    _log('Error en la consulta');
									              });
									    });
							    });
							});			        		
			        	}
			        	else
			        	{
                            logout_sistema(logueado.token, function(estatus) {
                            });
			        		callback(0);
			        	}

			        },
			        error: function(xhr) {
                        logout_sistema(logueado.token, function(estatus) {
                        });
			        	callback(0);
			        }
			    });

		}
        else if(logueado.estatus == -1)
        {
				callback(-1);
        }
	});

}


//Funcion que descarga los datos de las carreras a la bd local
function descarga_datos_carreras(callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
				$.ajax({
			        method: 'GET', 
			        url: url + '/Carreras?filter={"where":{"campusId":"'+campusid+'"},"order":"nombre ASC","fields":["id","nombre","icono","img_principal","contenido","modalidadId"],"include":{"relation":"modalidad_pertenece","scope":{"fields":["id","nombre"]} }}&access_token='+logueado.token,
			        dataType: 'json',
			        success: function(data)
			        {
							db.transaction( function(tx) {
							    tx.executeSql("DELETE FROM carreras", this.txErrorHandler, function(tx, results) {
					                	if(data.length == 0) {
						                		_log('carreras cargadas');
				                                logout_sistema(logueado.token, function(estatus) {
				                                });
						                		callback(1);
					                	}
					                	else
					                	{
												$$.each(data, function(index, rec)
												{
													    db.transaction(function (tx) {
													            tx.executeSql('INSERT INTO carreras (id, modalidad, nombre, icono, img_principal, contenido) VALUES (?,?,?,?,?,?)',
													              [rec.id, rec.modalidad_pertenece.nombre, rec.nombre, rec.icono, rec.img_principal, rec.contenido], function (tx, results)
													              {
													                	_log("cargando carrera " + (index + 1) + " de " + data.length);
													                	if((index+1) == data.length ) {
													                		_log('carreras cargadas');
											                                logout_sistema(logueado.token, function(estatus) {
											                                });
													                		callback(1);
													                	}
													              }, function (tx) {
													                    _log('Error en la consulta');
													              });
													    });
												});					                		
					                	}
							    });
							});
			        },
			        error: function(xhr) {
                        logout_sistema(logueado.token, function(estatus) {
                        });
			        	callback(0);
			        }
			    });

        }
        else if(logueado.estatus == -1)
        {
				callback(0);
        }
	});
}


//Funcion que descarga los datos de los videos a la bd local
function descarga_datos_videos(callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
				$.ajax({
			        method: 'GET', 
			        url: url + '/Galeria_videos?filter={"where":{"campusId":"'+campusid+'"},"order":"titulo ASC","fields":["id","titulo","fecha_creacion","url"]}&access_token='+logueado.token,
			        dataType: 'json',
			        success: function(data)
			        {
							db.transaction( function(tx) {
							    tx.executeSql("DELETE FROM galeria_videos", this.txErrorHandler, function(tx, results) {
										if(data.length == 0) {
												_log('videos cargados');
				                                logout_sistema(logueado.token, function(estatus) {
				                                });
												callback(1);
										}
										else
										{
												$$.each(data, function(index, rec)
												{
													    db.transaction(function (tx) {
													            tx.executeSql('INSERT INTO galeria_videos (id, titulo, fecha_creacion, url) VALUES (?,?,?,?)',
													              [rec.id, rec.titulo, rec.fecha_creacion, rec.url], function (tx, results)
													              {
																		_log("cargando video " + (index + 1) + " de " + data.length);
																		if((index+1) == data.length ) {
																			_log('videos cargados');
											                                logout_sistema(logueado.token, function(estatus) {
											                                });
																			callback(1);
																		}
													              }, function (tx) {
													                    _log('Error en la consulta');
													              });
													    });
												});											
										}
							    });
							});
			        },
			        error: function(xhr) {
                        logout_sistema(logueado.token, function(estatus) {
                        });
			        	callback(0);
			        }
			    });
        }
        else if(logueado.estatus == -1)
        {
				callback(0);
        }
	});

}

//Funcion que descarga la lista de secciones de UTRM conectado a la bd local
function descarga_lista_secciones_utrm(callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
				$.ajax({
			        method: 'GET', 
			        url: url + '/ClasifVideosUTRMs?filter={"where":{"campusId":"'+campusid+'"},"order":"nombre ASC","fields":["id","nombre"]}&access_token='+logueado.token,
			        dataType: 'json',
			        success: function(data)
			        {
							db.transaction( function(tx) {
							    tx.executeSql("DELETE FROM clasifvideos_utrm", this.txErrorHandler, function(tx, results) {
										if(data.length == 0) {
												_log('clasificacion videos UTRM cargados');
				                                logout_sistema(logueado.token, function(estatus) {
				                                });
												callback(1);
										}
										else
										{
												$$.each(data, function(index, rec)
												{
													    db.transaction(function (tx) {
													            tx.executeSql('INSERT INTO clasifvideos_utrm (id, descripcion) VALUES (?,?)',
													              [rec.id, rec.nombre], function (tx, results)
													              {
																		_log("cargando video UTRM " + (index + 1) + " de " + data.length);
																		if((index+1) == data.length ) {
																			_log('clasificacion videos UTRM cargados');
											                                logout_sistema(logueado.token, function(estatus) {
											                                });
																			callback(1);
																		}
													              }, function (tx) {
													                    _log('Error en la consulta');
													              });
													    });
												});											
										}
							    });
							});
			        },
			        error: function(xhr) {
                        logout_sistema(logueado.token, function(estatus) {
                        });
			        	callback(0);
			        }
			    });
        }
        else if(logueado.estatus == -1)
        {
				callback(0);
        }
	});

}

//Funcion que descarga los datos de los videos de UTRM Conectado a la bd local
function descarga_datos_videos_utrm(callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
				$.ajax({
			        method: 'GET', 
			        url: url + '/Galeria_videoUTRMs?filter={"where":{"campusId":"'+campusid+'"},"order":"titulo ASC","fields":["id","titulo","fecha_creacion","url","clasifId"]}&access_token='+logueado.token,
			        dataType: 'json',
			        success: function(data)
			        {
							db.transaction( function(tx) {
							    tx.executeSql("DELETE FROM galeria_videos_utrm", this.txErrorHandler, function(tx, results) {
										if(data.length == 0) {
												_log('videos UTRM cargados');
				                                logout_sistema(logueado.token, function(estatus) {
				                                });
												callback(1);
										}
										else
										{
												$$.each(data, function(index, rec)
												{
													    db.transaction(function (tx) {
													            tx.executeSql('INSERT INTO galeria_videos_utrm (id, clasifid, titulo, fecha_creacion, url) VALUES (?,?,?,?,?)',
													              [rec.id, rec.clasifId, rec.titulo, rec.fecha_creacion, rec.url], function (tx, results)
													              {
																		_log("cargando video UTRM " + (index + 1) + " de " + data.length);
																		if((index+1) == data.length ) {
																			_log('videos UTRM cargados');
											                                logout_sistema(logueado.token, function(estatus) {
											                                });
																			callback(1);
																		}
													              }, function (tx) {
													                    _log('Error en la consulta');
													              });
													    });
												});											
										}
							    });
							});
			        },
			        error: function(xhr) {
                        logout_sistema(logueado.token, function(estatus) {
                        });
			        	callback(0);
			        }
			    });
        }
        else if(logueado.estatus == -1)
        {
				callback(0);
        }
	});

}


//Funcion que descarga los datos de las fotos a la bd local
function descarga_datos_fotos(callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
				$.ajax({
			        method: 'GET', 
			        url: url + '/Galeria_fotos?filter={"where":{"campusId":"'+campusid+'"},"order":"titulo ASC","fields":["id","titulo","fecha_creacion","data"]}&access_token='+logueado.token,
			        dataType: 'json',
			        success: function(data)
			        {
							db.transaction( function(tx) {
							    tx.executeSql("DELETE FROM galeria_fotos", this.txErrorHandler, function(tx, results) {
										if(data.length == 0) {
												_log('fotos cargadas');
				                                logout_sistema(logueado.token, function(estatus) {
				                                });
												callback(1);
										}
										else
										{
												$$.each(data, function(index, rec)
												{
													    db.transaction(function (tx) {
													            tx.executeSql('INSERT INTO galeria_fotos (id, titulo, fecha_creacion, data) VALUES (?,?,?,?)',
													              [rec.id, rec.titulo, rec.fecha_creacion, rec.data], function (tx, results)
													              {
																		_log("cargando foto " + (index + 1) + " de " + data.length);
																		if((index+1) == data.length ) {
																			_log('fotos cargadas');
											                                logout_sistema(logueado.token, function(estatus) {
											                                });
																			callback(1);
																		}
													              }, function (tx) {
													                    _log('Error en la consulta');
													              });
													    });
												});											
										}
							    });
							});
			        },
			        error: function(xhr) {
                        logout_sistema(logueado.token, function(estatus) {
                        });
			        	callback(0);
			        }
			    });
        }
        else if(logueado.estatus == -1)
        {
				callback(0);
        }
	});

}


//Funcion que descarga los datos de los articulos a la bd local
function descarga_datos_articulos(callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
				$.ajax({
			        method: 'GET', 
			        url: url + '/Articulos?filter={"where":{"and":[{"campusId":"'+campusid+'"},{"activo":true}]},"order":"fecha DESC","fields":["id","titulo","descripcion","contenido","fecha","img_principal"]}&access_token='+logueado.token,
			        dataType: 'json',
			        success: function(data)
			        {
							db.transaction( function(tx) {
							    tx.executeSql("DELETE FROM articulos", this.txErrorHandler, function(tx, results) {

										if(data.length == 0) {
											_log('articulos cargados');
			                                logout_sistema(logueado.token, function(estatus) {
			                                });
											callback(1);
										}
										else
										{
												$$.each(data, function(index, rec)
												{
													    if(rec.img_principal == '' || rec.img_principal === undefined)
													        var img_principal = '';
													    else
													        var img_principal = rec.img_principal;

													    db.transaction(function (tx) {
													            tx.executeSql('INSERT INTO articulos (id, titulo, descripcion, contenido, fecha, img_principal) VALUES (?,?,?,?,?,?)',
													              [rec.id, rec.titulo, rec.descripcion, rec.contenido, rec.fecha, img_principal], function (tx, results)
													              {
																		_log("cargando articulo " + (index + 1) + " de " + data.length);
																		if((index+1) == data.length ) {
																			_log('articulos cargados');
											                                logout_sistema(logueado.token, function(estatus) {
											                                });
																			callback(1);
																		}
													              }, function (tx) {
													                    _log('Error en la consulta');
													              });
													    });
												});											
										}

							    });
							});
			        },
			        error: function(xhr) {
                        logout_sistema(logueado.token, function(estatus) {
                        });
			        	callback(0);
			        }
			    });
        }
        else if(logueado.estatus == -1)
        {
				callback(0);
        }
	});

}


//Funcion que descarga los datos de los eventos a la bd local y funciona como el primer evento para checar si hay conexion, de tal manera que evite que los otros se realicen sino hay conexion
function descarga_datos_eventos(callback)
{
	login_sistema(function(logueado) {

		if(logueado.estatus == 1)
		{
				$.ajax({
			        method: 'GET', 
			        url: url + '/Eventos?filter={"where":{"and":[{"campusId":"'+campusid+'"},{"activo":true}]},"order":"fecha_inicio DESC","fields":["nombre","ubicacion","descripcion","contenido","fechas_evento","img_principal","img_mapa_sitio","url_video","id"],"include":{"relation":"programa","scope":{"order":"orden ASC"} }}&access_token='+logueado.token,
			        dataType: 'json',
			        success: function(data)
			        {
							db.transaction( function(tx) {
										  tx.executeSql("DELETE FROM programa", this.txErrorHandler, function(tx, results) {
										  });
							});

							db.transaction( function(tx) {
							    tx.executeSql("DELETE FROM eventos", this.txErrorHandler, function(tx, results) {

					                	if(data.length == 0) {
						                		_log('eventos cargados');
				                                logout_sistema(logueado.token, function(estatus) {
				                                });
						                		callback(1);
					                	}
					                	else
					                	{
												$$.each(data, function(index, rec)
												{
													    if(rec.img_principal == '' || rec.img_principal === undefined)
													        var img_principal = 'images/articulo_default.jpg';
													    else
													        var img_principal = rec.img_principal;

													    if(rec.img_mapa_sitio == '' || rec.img_mapa_sitio === undefined)
													        var img_mapa_sitio = 'images/articulo_default.jpg';
													    else
													        var img_mapa_sitio = rec.img_mapa_sitio;

													    if(rec.url_video == '' || rec.url_video === undefined)
													        var url_video = '';
													    else
													        var url_video = rec.url_video;

													    db.transaction(function (tx) {
													            tx.executeSql('INSERT INTO eventos (id, nombre, ubicacion, descripcion, contenido, fechas_evento, img_principal, img_mapa_sitio, url_video) VALUES (?,?,?,?,?,?,?,?,?)',
													              [rec.id, rec.nombre, rec.ubicacion, rec.descripcion, rec.contenido, rec.fechas_evento, img_principal, img_mapa_sitio, url_video], function (tx, results)
													              {
													                	var d1 = $.Deferred();
													                	$$.each(rec.programa, function(index2, fila)
													                	{
																			    db.transaction(function (tx) {
																			            tx.executeSql('INSERT INTO programa (id, eventoid, orden, fecha, hora, actividad, ponente) VALUES (?,?,?,?,?,?,?)',
																			              [fila.id, fila.eventoId, fila.orden, fila.fecha, fila.hora, fila.actividad, fila.ponente], function (tx, results)
																			              {
																			              	
																			              	if( (index2+1) == rec.programa.length ){
																			              		_log('terminado programa del evento:',  rec.nombre);
																			              		d1.resolve( index );
																			              	}

																			              }, function (tx) {
																			                    _log('Error en la consulta');
																			              });
																			    });
													                	});
																		
																		$.when( d1 ).done(function ( v1 ) {
														                	_log("cargando evento " + (v1 + 1) + " de " + data.length);
														                	if((v1+1) == data.length ) {
														                		_log('eventos cargados');
												                                logout_sistema(logueado.token, function(estatus) {
												                                });
														                		callback(1);
														                	}
																		});
													              }, function (tx) {
													                    _log('Error en la consulta');
													              });
													    });
												});					                		
					                	}

							    });
							});
			        },
			        error: function(xhr) {
                        logout_sistema(logueado.token, function(estatus) {
                        });
			        	callback(0);
			        }
			    });
        }
        else if(logueado.estatus == -1)
        {
				callback(-1);
        }
	});

}



//Funcion auxiliar para hacer login y obtener el token para poder acceder a los datos
function login_sistema(callback)
{
    $$.ajax({
        method: 'POST',
        url: url + "/Usuarios/login",
        data: {
			username : "app_movil",
			password : "app_movil"
        },
        dataType: 'json',
        success: function(data)
        {
			callback({
				estatus : 1,
				token   : data.id
			});
        },
        error: function(xhr)
        {
            if(xhr.status == 401)
            {
                callback({
                    estatus: 0,
                    token: ''
                });
            }
            else if(xhr.status == 0)
            {
                callback({
                    estatus: -1,
                    token: ''
                });                    
            }
            else
            {
                callback({
                    estatus: -2,
                    token: ''
                });                    
            }
        }
    });
}


function logout_sistema(token_conexion, callback) 
{
      $$.ajax({
          method: 'POST',
          url: url + '/Usuarios/logout?access_token='+token_conexion,
          dataType: 'json',
          success: function(data)
          {
              callback(1);
          },
          error: function(xhr)
          {
              callback(0);
          }
      });

}


