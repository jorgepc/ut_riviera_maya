
SELECT
  dbo.alumnos.matricula AS clave_alumno,
  dbo.alumnos.nombre AS nombres,
  concat(dbo.alumnos.ap_paterno, ' ', dbo.alumnos.ap_materno) AS apellidos,
  dbo.cat_carreras.descripcion AS carrera,
  (CASE
  WHEN dbo.alumnos.grado = 1 THEN '1RO'
  WHEN dbo.alumnos.grado = 2 THEN '2DO'
  WHEN dbo.alumnos.grado = 3 THEN '3RO'
  WHEN dbo.alumnos.grado = 4 THEN '4TO'
  WHEN dbo.alumnos.grado = 5 THEN '5TO'
  WHEN dbo.alumnos.grado = 6 THEN '6TO'
  WHEN dbo.alumnos.grado = 7 THEN '7MO'
  WHEN dbo.alumnos.grado = 8 THEN '8VO'
  WHEN dbo.alumnos.grado = 9 THEN '9NO'
  WHEN dbo.alumnos.grado = 9 THEN '10MO'
  END) as semestre,

  dbo.cat_materias.descripcion AS materia,
  (CASE WHEN lunes IS NULL THEN '' ELSE lunes END) AS lunes,
  (CASE WHEN martes IS NULL THEN '' ELSE martes END) AS martes,
  (CASE WHEN miercoles IS NULL THEN '' ELSE miercoles END) AS miercoles,
  (CASE WHEN jueves IS NULL THEN '' ELSE jueves END) AS jueves,
  (CASE WHEN viernes IS NULL THEN '' ELSE viernes END) AS viernes,
  '' as sabado
FROM
  dbo.alumnos
  INNER JOIN dbo.cat_carreras ON (dbo.alumnos.carrera = dbo.cat_carreras.carrera)
  INNER JOIN dbo.grupo_alumnos ON (dbo.alumnos.matricula = dbo.grupo_alumnos.matricula)
  INNER JOIN dbo.grupos ON (dbo.grupo_alumnos.grupo = dbo.grupos.grupo)
  INNER JOIN dbo.grupos_materias ON (dbo.grupos.grupo = dbo.grupos_materias.grupo)
  INNER JOIN dbo.cat_materias ON (dbo.grupos_materias.materia = dbo.cat_materias.materia)
  INNER JOIN dbo.periodo_escolar ON (dbo.grupos.ciclo = dbo.periodo_escolar.ciclo)

  LEFT JOIN (SELECT grupo,materia, concat( concat(SUBSTRING(hora_inicio,1,2),':',SUBSTRING(hora_inicio,3,2)), ' - ', concat(SUBSTRING(hora_final,1,2),':',SUBSTRING(hora_final,3,2))) AS lunes FROM dbo.horario_grupo_materias WHERE dia_imparte = 1) as dia_lunes
  	ON (dbo.grupos_materias.grupo = dia_lunes.grupo) AND (dbo.grupos_materias.materia = dia_lunes.materia)
  LEFT JOIN (SELECT grupo,materia, concat( concat(SUBSTRING(hora_inicio,1,2),':',SUBSTRING(hora_inicio,3,2)), ' - ', concat(SUBSTRING(hora_final,1,2),':',SUBSTRING(hora_final,3,2))) AS martes FROM dbo.horario_grupo_materias WHERE dia_imparte = 2) as dia_martes
  	ON (dbo.grupos_materias.grupo = dia_martes.grupo) AND (dbo.grupos_materias.materia = dia_martes.materia)
  LEFT JOIN (SELECT grupo,materia, concat( concat(SUBSTRING(hora_inicio,1,2),':',SUBSTRING(hora_inicio,3,2)), ' - ', concat(SUBSTRING(hora_final,1,2),':',SUBSTRING(hora_final,3,2))) AS miercoles FROM dbo.horario_grupo_materias WHERE dia_imparte = 3) as dia_miercoles
  	ON (dbo.grupos_materias.grupo = dia_miercoles.grupo) AND (dbo.grupos_materias.materia = dia_miercoles.materia)
  LEFT JOIN (SELECT grupo,materia, concat( concat(SUBSTRING(hora_inicio,1,2),':',SUBSTRING(hora_inicio,3,2)), ' - ', concat(SUBSTRING(hora_final,1,2),':',SUBSTRING(hora_final,3,2))) AS jueves FROM dbo.horario_grupo_materias WHERE dia_imparte = 4) as dia_jueves
  	ON (dbo.grupos_materias.grupo = dia_jueves.grupo) AND (dbo.grupos_materias.materia = dia_jueves.materia)
  LEFT JOIN (SELECT grupo,materia, concat( concat(SUBSTRING(hora_inicio,1,2),':',SUBSTRING(hora_inicio,3,2)), ' - ', concat(SUBSTRING(hora_final,1,2),':',SUBSTRING(hora_final,3,2))) AS viernes FROM dbo.horario_grupo_materias WHERE dia_imparte = 5) as dia_viernes
  	ON (dbo.grupos_materias.grupo = dia_viernes.grupo) AND (dbo.grupos_materias.materia = dia_viernes.materia)

WHERE
  dbo.periodo_escolar.situacion = 1 AND
  dbo.alumnos.situacion IN (1,4,5,8,10)
  AND dbo.alumnos.matricula = '1301068'